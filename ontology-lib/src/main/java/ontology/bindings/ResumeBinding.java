package ontology.bindings;

import java.util.Map;
import java.util.Set;

public class ResumeBinding {
    private Map<String, Set<String>> educations;
    private Map<String, Set<String>> skills;
    private Map<String, Set<String>> possibleSkills;
    private String username;
    private String addinfo;

    public Map<String, Set<String>> getEducations() {
        return educations;
    }

    public void setEducations(Map<String, Set<String>> educations) {
        this.educations = educations;
    }

    public Map<String, Set<String>> getSkills() {
        return skills;
    }

    public void setSkills(Map<String, Set<String>> skills) {
        this.skills = skills;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAddinfo() {
        return addinfo;
    }

    public void setAddinfo(String addinfo) {
        this.addinfo = addinfo;
    }

    public Map<String, Set<String>> getPossibleSkills() {
        return possibleSkills;
    }

    public void setPossibleSkills(Map<String, Set<String>> possibleSkills) {
        this.possibleSkills = possibleSkills;
    }
}

package ontology.service;

import gate.util.GateException;
import ontology.bindings.ResumeBinding;

import java.io.FileNotFoundException;
import java.util.Map;

class TestService {
    public static void main(String[] args) throws GateException {
        OntologyService instance = OntologyService.getInstance();
        try {
            if(!instance.getOntologyManager().addSkill("testSkill", "object-oriented_programming")){
                Map<String, String> map = instance.getOntologyManager().findSkillParent("object-oriented_programming");
                instance.getOntologyManager().addSkillWithParent("testSkill", "object-oriented_programming", map.get("object-oriented_programming"));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ResumeBinding resume = instance.processText("testSkill");
        instance.destroy();
    }
}

package ontology.service;

import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.WordNetDatabase;
import gate.Corpus;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;
import ontology.bindings.ResumeBinding;
import ontology.service.annie.*;
import ontology.service.lang.LanguageIdentifier;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class OntologyService {
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(OntologyService.class);
    private static volatile OntologyService instance;
    private SerialAnalyserController annieController;

    private OntologyService() throws GateException {

        FeatureMap features = Factory.newFeatureMap();

        ServiceHelper.initGate();

        ServiceHelper.loadAnnie();

        ServiceHelper.initWordNet();

        annieController = ServiceHelper.initAnnie();

        features.clear();
        annieController.add(DocumentReset.PR(features));

        features.clear();
        annieController.add(LanguageIdentifier.PR(features));

        features.clear();
        annieController.add(Tokenizer.PR(features));

        features.clear();
        annieController.add(SentenceSplitter.PR(features));

        features.clear();
        annieController.add(POSTagger.PR(features));

        features.clear();
        annieController.add(Morpher.PR(features));

        features.clear();
        annieController.add(DefaultGazetteer.PR(features));
        features.put("listPR", annieController.getPRs());
        features.put("ontologyFilePath", OntologyManager.ONTOLOGY_FILE_PATH);
        annieController.add(Gazetteer.PR(features));

        features.clear();
        features.put("grammarURL", "resources/NE/main.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/skills.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/nouns.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/do_not.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/would_like_to.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/will.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/negative_modal.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/edu_org.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/degree.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/good_at.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/have_experience.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/i_know.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/modal_verb_skill.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/write_in.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/rock_at.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/the_best_at.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/worked_with.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/education_via_edu_org.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        annieController.add(AliasMatcher.PR(features));

        features.clear();
        annieController.add(PronominalCoreference.PR(features));

	    /* Testing that all PRs have the required parameters to run */
        System.out.println("Testing PRs");
        annieController.getOffendingPocessingResources();

    }

    public static OntologyService getInstance() {
        if (instance == null) {
            synchronized (OntologyService.class) {
                if (instance == null) {
                    try {
                        instance = new OntologyService();
                    } catch (GateException e) {
                        LOGGER.error(e.toString());
                        throw new RuntimeException(e);
                    }
                }
            }
        }
        return instance;
    }

    public ResumeBinding processText(String text) throws ResourceInstantiationException, ExecutionException {
        Corpus corpus = Factory.newCorpus("myCorpus");
        Document doc = Factory.newDocument(text);
        corpus.add(doc);
        annieController.setCorpus(corpus);
        annieController.execute();

        ResumeBinding resumeBinding = ServiceHelper.getResume(doc);
        Map<String, String> skillsWithParent = getOntologyManager().findAllSkillParent();
        for(String s: text.split("\\W+")){
             if(skillsWithParent.containsKey(s)){
                 if(!resumeBinding.getSkills().containsKey(skillsWithParent.get(s))){
                     Set<String> skill = new HashSet();
                     skill.add(s);
                     resumeBinding.getSkills().put(skillsWithParent.get(s), skill);
                 }
                 else{
                    Set<String> skills =  resumeBinding.getSkills().get(skillsWithParent.get(s));
                     skills.add(s);
                 }
            }
        }
        ResumeBinding nonSkillsRes = ServiceHelper.getNonSkills(doc);
        for(String parent: nonSkillsRes.getSkills().keySet()){
            for(String skill : nonSkillsRes.getSkills().get(parent)){
                resumeBinding.getSkills().get(parent).remove(skill);
            }
        }
        Set<String> nonModelSkills = ServiceHelper.getNonModelSkills(ServiceHelper.getPossibleSkills(doc));
        Map<String, Set<String>> nonModelSkillsWithParents = new HashMap<>();
        List<String> toRemove = new ArrayList<>();
        for(String nonModelSkill: nonModelSkills){
            for(String parent: resumeBinding.getSkills().keySet()){
                toRemove.addAll(resumeBinding.getSkills().get(parent).stream().filter(skill -> skill.equalsIgnoreCase(nonModelSkill)).map(skill -> nonModelSkill).collect(Collectors.toList()));
            }
        }
        nonModelSkills.removeAll(toRemove);
        for(String nonModelSkill: nonModelSkills){
            Set potentialParents = findParents(executeWordNet(nonModelSkill));
            nonModelSkillsWithParents.put(nonModelSkill, potentialParents);
        }
        resumeBinding.setPossibleSkills(nonModelSkillsWithParents);
        //Clean up
        Factory.deleteResource(doc);
        Factory.deleteResource(corpus);

        return resumeBinding;

    }

    public String executeWordNet(String skill){
        StringBuilder stringBuilder = new StringBuilder();
        WordNetDatabase database = WordNetDatabase.getFileInstance();
        Synset[] synsets = database.getSynsets(skill, SynsetType.NOUN);
        if (synsets.length > 0) {
            for (Synset synset : synsets) {
                stringBuilder.append(synset.getDefinition()).append('\n');
            }
        }
        return stringBuilder.toString();
    }

    public Set findParents(String definitionSkills) throws ExecutionException, ResourceInstantiationException {
        Set set = new HashSet<>();
       if(definitionSkills.length() > 0){
           Corpus corpus = Factory.newCorpus("myCorpus");
           Document doc = Factory.newDocument(definitionSkills);
           corpus.add(doc);
           annieController.setCorpus(corpus);
           annieController.execute();

           set = ServiceHelper.getParents(doc);

           //Clean up
           Factory.deleteResource(doc);
           Factory.deleteResource(corpus);

           }
        return set;
    }

    public OntologyManager getOntologyManager() {
        return OntologyManager.getInstance();
    }

    public void destroy() {
        synchronized (OntologyService.class) {
//            for (ProcessingResource pr : annieController.getPRs()) {
//                Factory.deleteResource(pr);
//            }
            Factory.deleteResource(annieController);
            annieController = null;
            instance = null;
        }
    }



}

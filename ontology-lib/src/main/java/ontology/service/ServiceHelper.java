package ontology.service;

import gate.*;
import gate.creole.ResourceInstantiationException;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;
import gate.util.Out;
import ontology.bindings.ResumeBinding;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.MalformedURLException;
import java.util.*;
import java.util.stream.Collectors;

class ServiceHelper {
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ServiceHelper.class);
    static final String WEBAPP_ROOT = System.getProperty("webapp.root");
    static final String RESOURCE_PATH = StringUtils.isEmpty(WEBAPP_ROOT) ? String.format("%s/webservice/src/main/resources/ontology-lib/resources/", System.getProperty("user.dir")) :
           WEBAPP_ROOT.charAt(WEBAPP_ROOT.length()-1) == '/'? String.format("%sWEB-INF/classes/ontology-lib/resources/",WEBAPP_ROOT):
                   String.format("%s/WEB-INF/classes/ontology-lib/resources/",WEBAPP_ROOT);
    private static final String GATE_PLUGINS_HOME = "gate.plugins.home";
    public static final String WORDNET_DATABASE_DIR = "wordnet.database.dir";
//    public static final String GATE_HOME = StringUtils.isEmpty(System.getProperty("webapp.root")) ? Gate.getUrl().getPath() : System.getProperty("webapp.root");
    private static File gateHome;
    private static File pluginsHome;

    static void initGate() throws GateException {
        BasicConfigurator.configure();/* Set log4j.properties path */
        if (!Gate.isInitialised()) {
            Out.prln("Initialising GATE...");

//            System.setProperty(GATE_PLUGINS_HOME, String.format("%splugins", RESOURCE_PATH));
//            System.setProperty("gate.site.config", String.format("%sgate.xml", RESOURCE_PATH));

//            Gate.setPluginsHome(new File(String.format("%splugins", RESOURCE_PATH)));
//            Gate.setSiteConfigFile(new File(String.format("%sgate.xml", RESOURCE_PATH)));

//            Gate.setPluginsHome(new File(ServiceHelper.class.getClassLoader().getResource("plugins").getPath()));
//            Gate.setSiteConfigFile(new File(ServiceHelper.class.getClassLoader().getResource("gate.xml").getFile()));
            Gate.runInSandbox(true);

            Gate.init();
//            gateHome = new File(Gate.class.getResource("Gate.class").getPath());
            gateHome = new File(Gate.getUrl().getPath());
            pluginsHome = new File(String.format("%splugins", RESOURCE_PATH));
            if (Gate.getGateHome() == null) {
                Gate.setGateHome(gateHome);
            }else{
                gateHome = Gate.getGateHome();
            }
            if (Gate.getPluginsHome() == null) {
                Gate.setPluginsHome(pluginsHome);
            }else {
                pluginsHome = Gate.getPluginsHome();
            }
            try {

                Gate.getCreoleRegister().registerDirectories(
                        new File(pluginsHome, "ANNIE").toURI().toURL());
                Gate.getCreoleRegister().registerDirectories(
                        new File(pluginsHome, "Ontology_Tools").toURI().toURL());
                Gate.getCreoleRegister().registerDirectories(
                        new File(pluginsHome, "Ontology").toURI().toURL());
                Gate.getCreoleRegister().registerDirectories(
                        new File(pluginsHome, "Gazetteer_Ontology_Based").toURI().toURL());
                Gate.getCreoleRegister().registerDirectories(
                        new File(pluginsHome, "Tools").toURI().toURL());
                Gate.getCreoleRegister().registerDirectories(
                        new File(pluginsHome, "WordNet").toURI().toURL());
                Out.prln("...GATE initialised");
            } catch (GateException | MalformedURLException e) {
                LOGGER.error(e.toString());
            }
        }
    }

    static void loadAnnie() {
        LOGGER.info("\nLoading ANNIE...");
        File pluginsHome = Gate.getPluginsHome();
        try {
            Gate.getCreoleRegister().registerDirectories(
                    new File(pluginsHome, "ANNIE").toURL());
        } catch (MalformedURLException | GateException ex) {
            LOGGER.error(ex.toString());
        }
    }

    public static void initWordNet() {
        System.setProperty(WORDNET_DATABASE_DIR, String.format("%sWordNet-3.0/dict", RESOURCE_PATH));
    }

    static SerialAnalyserController initAnnie()
            throws ResourceInstantiationException {
        LOGGER.info("\nInitialising ANNIE...");
        return (SerialAnalyserController) Factory
                .createResource("gate.creole.SerialAnalyserController",
                        Factory.newFeatureMap(), Factory.newFeatureMap(),
                        "ANNIE_" + Gate.genSym());
    }

    static ResumeBinding getResume(Document doc){
        ResumeBinding resumeBinding= new ResumeBinding();
        AnnotationSet defaultAnnotSet = doc.getAnnotations();
        resumeBinding.setEducations(getEducations(defaultAnnotSet.get("EducationViaEduOrg"), defaultAnnotSet.get("EduOrg")));
        resumeBinding.setSkills(getSkills(defaultAnnotSet.get("Skills"), getNotHaveSkillsAnnotSet(defaultAnnotSet)));
        return resumeBinding;
    }
    static ResumeBinding getNonSkills(Document doc){
        ResumeBinding resumeBinding= new ResumeBinding();
        AnnotationSet defaultAnnotSet = doc.getAnnotations();
        resumeBinding.setSkills(getNonSkills(getNotHaveSkillsAnnotSet(defaultAnnotSet)));
        return resumeBinding;
    }

    static Set getParents(Document doc){
        AnnotationSet defaultAnnotSet = doc.getAnnotations();
        return findSkillsByDefinitions(getWordNetSkills(defaultAnnotSet));
    }

    private static AnnotationSet getNotHaveSkillsAnnotSet(AnnotationSet defaultAnnotSet){
        Set<String> annotTypesRequired = new HashSet();
        annotTypesRequired.add("Do_not");
        annotTypesRequired.add("Would_like_to");
        annotTypesRequired.add("Will");
        annotTypesRequired.add("Negative_modal");
        return defaultAnnotSet.get(annotTypesRequired);
    }

    private static AnnotationSet getWordNetSkills(AnnotationSet defaultAnnotSet){
        Set<String> annotTypesRequired = new HashSet();
        annotTypesRequired.add("Skills");
        annotTypesRequired.add("skills-ontology");
        annotTypesRequired.add("skills");
        return defaultAnnotSet.get(annotTypesRequired);
    }

    public static AnnotationSet getPossibleSkills(Document doc){
        AnnotationSet defaultAnnotSet = doc.getAnnotations();
        Set<String> annotTypesRequired = new HashSet();
        annotTypesRequired.add("Good_at");
        annotTypesRequired.add("Have_experience");
        annotTypesRequired.add("I_know");
        annotTypesRequired.add("Write_in");
        annotTypesRequired.add("Modal_verb_skill");
        annotTypesRequired.add("Rock_at");
        annotTypesRequired.add("The_best_at");
        annotTypesRequired.add("Worked_with");
        return defaultAnnotSet.get(annotTypesRequired);
    }

    private static Map<String, Set<String>> getEducations(AnnotationSet educationViaEduOrg, AnnotationSet eduOrg) {
        final Map<String, Set<String>> educations = new LinkedHashMap<>();
        educations.put("PhD", new HashSet<>());
        educations.put("Courses", new HashSet<>());
        educations.put("Master", new HashSet<>());
        educations.put("Bachelor", new HashSet<>());
        educations.put("School", new HashSet<>());

        educationViaEduOrg.stream()
                .forEach(annotation -> {
                    FeatureMap featureMap = annotation.getFeatures();
                    Set<String> set = educations.get(featureMap.get("degree"));
                    if (set == null) {
                        set = new HashSet<>();
                        educations.put((String)featureMap.get("degree"), set);
                    }
                    set.addAll(getEduOrgFullNameSet(featureMap, eduOrg));
                });

        eduOrg.stream()
                .filter(annotation -> {
                    for (Set valSet : educations.values()) {
                        if (valSet.contains(annotation.getFeatures().get("fullName"))) {
                            return false;
                        }
                    }
                    return true;
                })
                .forEach(annotation -> {
                    FeatureMap featureMap = annotation.getFeatures();
                    Set<String> set = educations.get(featureMap.get("kind"));
                    if (set == null) {
                        set = new HashSet<>();
                        educations.put((String) featureMap.get("kind"), set);
                    }
                    set.add((String) featureMap.get("fullName"));
                });


        return educations;
    }

    private static Set<String> getEduOrgFullNameSet(FeatureMap featureMap, AnnotationSet eduOrg) {
        String eduOrgFullName = (String) featureMap.get("organization");
        if (eduOrgFullName != null) {
            Set<String> resutSet = new HashSet<>();
            resutSet.add(eduOrgFullName);
            return resutSet;
        } else {
            return eduOrg.stream()
                    .filter(annotation ->
                            ((String)annotation.getFeatures().get("fullName")).contains((String)featureMap.get("degree")))
                    .map(annotation->(String)annotation.getFeatures().get("fullName"))
                    .collect(Collectors.toSet());
        }
    }

    public static Set<String> getNonModelSkills(AnnotationSet nounsAnnotSet){
         return nounsAnnotSet.stream()
                 .filter(annotation -> annotation.getFeatures().get("skill") != null )
                 .map(annotation -> ((String) annotation.getFeatures().get("skill")))
                 .collect(Collectors.toSet());
    }

    static Map<String, Set<String>> getSkills(AnnotationSet allSkillsAnnotSet, AnnotationSet notHaveSkillsAnnotSet) {
        Set skills = allSkillsAnnotSet.stream()
                .filter(annotation -> annotation.getFeatures().get("URI") != null && ((String) annotation.getFeatures().get("URI")).contains("skills-ontology"))
                .map(annotation -> {
                    String str = ((String) annotation.getFeatures().get("URI"));
                    return str.substring(str.indexOf("#") + 1);
                })
                .collect(Collectors.toSet());

        Set notHaveSkills = notHaveSkillsAnnotSet.stream()
                .filter(annotation -> annotation.getFeatures().get("URI") != null && ((String) annotation.getFeatures().get("URI")).contains("skills-ontology"))
                .map(annotation -> {
                    String str = ((String) annotation.getFeatures().get("URI"));
                    return str.substring(str.indexOf("#") + 1);
                })
                .collect(Collectors.toSet());

        skills.removeAll(notHaveSkills);

        return OntologyManager.getInstance().findParent(skills);
    }

    static Map<String, Set<String>> getNonSkills(AnnotationSet notHaveSkillsAnnotSet){
        Set notHaveSkills = notHaveSkillsAnnotSet.stream()
                .filter(annotation -> annotation.getFeatures().get("URI") != null && ((String) annotation.getFeatures().get("URI")).contains("skills-ontology"))
                .map(annotation -> {
                    String str = ((String) annotation.getFeatures().get("URI"));
                    return str.substring(str.indexOf("#") + 1);
                })
                .collect(Collectors.toSet());
        return OntologyManager.getInstance().findParent(notHaveSkills);
    }

    private static Set findSkillsByDefinitions(AnnotationSet skillsAnnotSet) {
        Set set = skillsAnnotSet.stream()
                .filter(annotation -> annotation.getFeatures().get("URI") != null && ((String) annotation.getFeatures().get("URI")).contains("skills-ontology"))
                .map(annotation -> {
                    String str = ((String) annotation.getFeatures().get("URI"));
                    return str.substring(str.indexOf("#") + 1);
                })
                .collect(Collectors.toSet());
            return set;
    }
}

package ontology.service.example;

import com.hp.hpl.jena.rdf.model.*;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.util.FileManager;
import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.WordNetDatabase;
import gate.*;
import gate.corpora.DocumentStaxUtils;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;
import gate.util.Out;
import ontology.bindings.ResumeBinding;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import javax.xml.stream.XMLStreamException;
import java.io.*;
import java.net.MalformedURLException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author Igor Sila
 */
class InitApp {
    private static Logger LOGGER = Logger.getRootLogger();
    private static SerialAnalyserController annieController;
    private static String RESOURCE_PATH = "src/main/resources/";
    private static String GATE_PLUGINS_HOME = "gate.plugins.home";
    public static Document doc = null;
    private static File gateHome;
    private static File pluginsHome;
    private static  Model model = ModelFactory.createDefaultModel();

    static void initGate() throws GateException {
        System.setProperty(GATE_PLUGINS_HOME, String.format("%splugins", RESOURCE_PATH));
        BasicConfigurator.configure();/* Set log4j.properties path */
        if (!Gate.isInitialised()) {
            Out.prln("Initialising GATE...");
            System.setProperty("gate.site.config",String.format("%sgate.xml", RESOURCE_PATH));
            Gate.init();
            gateHome = Gate.getGateHome();
            pluginsHome = new File(System.getProperty(GATE_PLUGINS_HOME));
            if (Gate.getGateHome() == null) {
                Gate.setGateHome(gateHome);
            }
            if (Gate.getPluginsHome() == null) {
                Gate.setPluginsHome(pluginsHome);
            }
            try {

                Gate.getCreoleRegister().registerDirectories(
                        new File(pluginsHome, "ANNIE").toURI().toURL());
                Gate.getCreoleRegister().registerDirectories(
                        new File(pluginsHome, "Ontology_Tools").toURI().toURL());
                Gate.getCreoleRegister().registerDirectories(
                        new File(pluginsHome, "Ontology").toURI().toURL());
                Gate.getCreoleRegister().registerDirectories(
                        new File(pluginsHome, "Gazetteer_Ontology_Based").toURI().toURL());
                Gate.getCreoleRegister().registerDirectories(
                        new File(pluginsHome, "Tools").toURI().toURL());
                Gate.getCreoleRegister().registerDirectories(
                        new File(pluginsHome, "WordNet").toURI().toURL());
                Out.prln("...GATE initialised");
            } catch (GateException | MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("deprecation")
    static void loadAnnie() {
        LOGGER.info("\nLoading ANNIE...");
        File pluginsHome = Gate.getPluginsHome();
        try {
            Gate.getCreoleRegister().registerDirectories(
                    new File(pluginsHome, "ANNIE").toURL());
        } catch (MalformedURLException | GateException ex) {
            ex.printStackTrace();
        }
    }

    static SerialAnalyserController initAnnie()
            throws ResourceInstantiationException {
        LOGGER.info("\nInitialising ANNIE...");
        return setAnnieController((SerialAnalyserController) Factory
                .createResource("gate.creole.SerialAnalyserController",
                        Factory.newFeatureMap(), Factory.newFeatureMap(),
                        "ANNIE_" + Gate.genSym()));
    }

    private static SerialAnalyserController setAnnieController(
            SerialAnalyserController annieController) {
        InitApp.annieController = annieController;
        return annieController;
    }

    static Corpus initCorpus(File f) throws MalformedURLException {
        Corpus corpus = null;

        FeatureMap params = Factory.newFeatureMap();
        params.put("sourceUrl", f.toURL());
        params.put("markupAware", true);
        params.put("preserveOriginalContent", false);
        params.put("collectRepositioningInfo", false);
        params.put("encoding", "windows-1252");
        Out.prln("Creating doc for " + f);
        try {
            corpus = Factory.newCorpus("myCorpus");
            doc = (Document) Factory.createResource(
                    "gate.corpora.DocumentImpl", params);

        } catch (ResourceInstantiationException ex) {
            ex.printStackTrace();
        }
        corpus.add(doc);

        if (corpus == null)
            Out.prln("Corpus is empty");
        return corpus;
    }

    private static Set<String> getEduOrgFullNameSet(FeatureMap featureMap, AnnotationSet eduOrg) {
        String eduOrgFullName = (String) featureMap.get("organization");
        if (eduOrgFullName != null) {
            Set<String> resutSet = new HashSet<>();
            resutSet.add(eduOrgFullName);
            return resutSet;
        } else {
            return eduOrg.stream()
                    .filter(annotation ->
                       ((String)annotation.getFeatures().get("fullName")).contains((String)featureMap.get("degree")))
                    .map(annotation->(String)annotation.getFeatures().get("fullName"))
                    .collect(Collectors.toSet());
        }
    }

    private static Map<String, Set<String>> getEducations(AnnotationSet educationViaEduOrg, AnnotationSet eduOrg) {
        final Map<String, Set<String>> educations = new HashMap<>();
        educationViaEduOrg.stream()
                .forEach(annotation -> {
                    FeatureMap featureMap = annotation.getFeatures();
                    Set<String> set = educations.get(featureMap.get("degree"));
                    if (set == null) {
                        set = new HashSet<>();
                        educations.put((String)featureMap.get("degree"), set);
                    }
                    set.addAll(getEduOrgFullNameSet(featureMap, eduOrg));
                });

        eduOrg.stream()
                .filter(annotation -> {
                    for (Set valSet : educations.values()) {
                        if (valSet.contains(annotation.getFeatures().get("fullName"))) {
                            return false;
                        }
                    }
                    return true;
                })
                .forEach(annotation -> {
                    FeatureMap featureMap = annotation.getFeatures();
                    Set<String> set = educations.get(featureMap.get("kind"));
                    if (set == null) {
                        set = new HashSet<>();
                        educations.put((String) featureMap.get("kind"), set);
                    }
                    set.add((String) featureMap.get("fullName"));
                });


        return educations;
    }

    static Map<String, Set<String>> getSkills(AnnotationSet skills) {
        Set set = skills.stream()
                .filter(annotation -> annotation.getFeatures().get("URI") != null && ((String) annotation.getFeatures().get("URI")).contains("skills-ontology"))
                .map(annotation -> {
                    String str = ((String) annotation.getFeatures().get("URI"));
                    return str.substring(str.indexOf("#") + 1);
                })
                .collect(Collectors.toSet());
        return findParent(set);
    }

    private static void printSkill() {
        AnnotationSet LookupAnnotSet = doc.getAnnotations();
//        List<File> files = new ArrayList<>();
        Set set = LookupAnnotSet.stream()
                .filter(annotation -> annotation.getFeatures().get("URI") != null && ((String) annotation.getFeatures().get("URI")).contains("skills-ontology"))
                .map(annotation -> {
                    String str = ((String) annotation.getFeatures().get("URI"));
                    return str.substring(str.indexOf("#") + 1);
                })
                .collect(Collectors.toSet());
        findParent(set);
    }
    private static Map<String, Set<String>> findParent(Set set){
        try(InputStream in = FileManager.get().open("src/main/resources/skills.owl")) {

            if (in == null) {
                throw new IllegalArgumentException("File: not found");
            }
            model.read(in, null);
            Map<String, String> map = getSuperClasses(model);
            Set<String> parents = new HashSet<>();
            Map<String, Set<String>> skillsWithParent = new HashMap<>();
            for (Object o : set) {
                String parent = map.get(o.toString());
                parents.add(parent);
            }
            for (Object o : parents) {
                Set<String> subSkills = new HashSet<>();
                for (Object s : set) {
                    if (o.toString().equals(map.get(s.toString()))) {
                        subSkills.add(s.toString());
                    }
                }
                skillsWithParent.put(o.toString(), subSkills);
            }
            printSkillsWithParent(skillsWithParent);
            return skillsWithParent;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static void printSkillsWithParent(Map<String, Set<String>> skillsWithParent){
        for(String s: skillsWithParent.keySet()){
            System.out.print(s + ": ");
            Set<String> skills =  skillsWithParent.get(s);
            System.out.println(skills);
        }
    }
    public static void initWordNet() {
        System.setProperty("wordnet.database.dir", String.format("%sWordNet-3.0/dict", RESOURCE_PATH));
    }

    static File executeWordNet(String word) {
        WordNetDatabase database = WordNetDatabase.getFileInstance();
        File file = null;
        Synset[] synsets = database.getSynsets(word, SynsetType.NOUN);
        if (synsets.length > 0) {
            System.out.println("***********************************************************************");
            System.out.println("The following synsets contain '" +
                    word + "' or a possible base form " +
                    "of that text:");
            file = new File(word + ".txt");
            file.deleteOnExit();
            for (Synset synset : synsets) {
                System.out.println("");
                String[] wordForms = synset.getWordForms();
                for (int j = 0; j < wordForms.length; j++) {
                    System.out.print((j > 0 ? ", " : "") +
                            wordForms[j]);
                }
                System.out.println(": " + synset.getDefinition());
                try {
                    FileUtils.write(file, synset.getDefinition() + "\n", true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return file;
    }

    static void printResults() {
        AnnotationSet defaultAnnotSet = doc.getAnnotations();
        Set<String> annotTypesRequired = new HashSet<String>();
//        annotTypesRequired.add("Number");
//        annotTypesRequired.add("Person");
//        annotTypesRequired.add("Location");
        annotTypesRequired.add("Organization");
//        annotTypesRequired.add("Date");
//        annotTypesRequired.add("Address");
//        annotTypesRequired.add("Title");
//        annotTypesRequired.add("Name");
//        annotTypesRequired.add("Lookup");
//        annotTypesRequired.add("Skills");
//        annotTypesRequired.add("Do_not");
//        annotTypesRequired.add("Would_like_to");
//        annotTypesRequired.add("Will");
//        annotTypesRequired.add("Negative_modal");
        annotTypesRequired.add("EduOrg");
        annotTypesRequired.add("Degree");
        annotTypesRequired.add("EducationViaEduOrg");
//        annotTypesRequired.add("Sentence");
//        annotTypesRequired.add("Split");
        AnnotationSet token = defaultAnnotSet.get(annotTypesRequired);

        // output XML to console
        String xmlDocument = doc.toXml(token, true);
        //Out.print(xmlDocument);

        // Generate XML File
        File f = new File("StANNIE_" + 1 + ".HTML");
        try {
            DocumentStaxUtils.writeDocument(doc, f);
        } catch (XMLStreamException | IOException e) {
            e.printStackTrace();
        }
        printSkill();

        System.out.println("Education: \n" + getEducations(defaultAnnotSet.get("EducationViaEduOrg"), defaultAnnotSet.get("EduOrg")));
    }

    static ResumeBinding getResume(Document doc){
        ResumeBinding resumeBinding= new ResumeBinding();

        AnnotationSet defaultAnnotSet = doc.getAnnotations();

        resumeBinding.setEducations(getEducations(defaultAnnotSet.get("EducationViaEduOrg"), defaultAnnotSet.get("EduOrg")));
        resumeBinding.setSkills(getSkills(defaultAnnotSet.get("Skills")));
        return resumeBinding;
    }

    static void findDefinitions(String skill){
        File f = executeWordNet(skill);
        try {
                annieController.setCorpus(InitApp.initCorpus(f));
                annieController.execute();
                InitApp.printResultsForDefinitions();
            } catch (MalformedURLException | ExecutionException e) {
                e.printStackTrace();
            }
    }

    static void printResultsForDefinitions() {
        AnnotationSet defaultAnnotSet = doc.getAnnotations();
        Set<String> annotTypesRequired = new HashSet<>();
        annotTypesRequired.add("skills-ontology");
        annotTypesRequired.add("Skills");
        annotTypesRequired.add("Do_not");
        annotTypesRequired.add("Would_like_to");
        annotTypesRequired.add("Will");
        AnnotationSet token = defaultAnnotSet.get(annotTypesRequired);

        // output XML to console
        doc.toXml(token, true);

        findSkillsByDefinitions();

    }

    static void findSkillsByDefinitions() {
        AnnotationSet LookupAnnotSet = doc.getAnnotations();
        List list = LookupAnnotSet.stream()
                .filter(annotation -> annotation.getFeatures().get("URI") != null && ((String) annotation.getFeatures().get("URI")).contains("skills-ontology"))
                .map(annotation -> {
                    String str = ((String) annotation.getFeatures().get("URI"));
                    return str.substring(str.indexOf("#") + 1);
                })
                .collect(Collectors.toList());
        if (list.size() > 0) {
            System.out.println("Skills find by parsing definitions that was found by WordNet: " + list);
        }

    }

    static void addSkill(String skill, String parent) throws FileNotFoundException {
        InputStream in = FileManager.get().open("src/main/resources/skills.owl");
        if (in == null) {
            throw new IllegalArgumentException("File: not found");
        }
        Statement s = null;
        Resource r = null;
        boolean once = false;
        String subEntry = "http://www.semanticweb.org/ontologies/skills-ontology#";
        String type = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
        model.read(in, null);
        StmtIterator iter = model.listStatements();
        while (iter.hasNext()) {
            Statement stmt = iter.nextStatement(); // get next statement
            Property predicate = stmt.getPredicate(); // get the predicate
            RDFNode object = stmt.getObject(); // get the object
            object.getClass().getSuperclass();

            if (object.toString().equals(subEntry+parent)) {
                r = model.createResource(subEntry + skill);
                s = model.createStatement(r, predicate, object);
                model.add(s);
            }
            else if(s != null && predicate.toString().equals(type) && !once) {
                r.addProperty(predicate, object);
                once = true;
                File f = new File("skills.owl");
                model.write(new FileOutputStream(f), null);
            }
        }
    }

    static String getLang() {
        return doc.getFeatures().get("lang") == null ? "unknown" : doc
                .getFeatures().get("lang").toString();
    }

    private static Map<String, String> getSuperClasses(Model model) {

        Map<String, String> map = new TreeMap<>();
        StmtIterator iter = model.listStatements();
        // print out the predicate, subject and object of each statement
        while (iter.hasNext()) {
            Statement stmt = iter.nextStatement(); // get next statement
            Resource subject = stmt.getSubject(); // get the subject
            Property predicate = stmt.getPredicate(); // get the predicate
            RDFNode object = stmt.getObject(); // get the object
            object.getClass().getSuperclass();
            if (predicate.toString().contains("subClassOf")) {
                String concept = getConceptName(subject.toString());
                String parent = getConceptName(object.toString());
                map.put(concept, parent);
            }
        }
        return map;
    }
    private static String getConceptName(String concept) {
        String regex = "#.*";
        Matcher m;
        Pattern pattern = Pattern.compile(regex);
        m = pattern.matcher(concept);
        if (m.find())
            return m.group(0).substring(1, m.group(0).length());
        return null;
    }
}

package ontology.service.example;

import gate.Corpus;
import gate.Factory;
import gate.FeatureMap;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import gate.creole.SerialAnalyserController;
import gate.persist.PersistenceException;
import gate.util.GateException;
import ontology.service.annie.*;
import ontology.service.lang.LanguageIdentifier;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

/**
 * @author Igor Sila
 */
class App {
    public static SerialAnalyserController annieController;
    private static FeatureMap features = Factory.newFeatureMap();
    private static Corpus corpus;

    public static void main(String[] args) throws GateException, IOException {
        annotate("message.txt");
    }

    public static void annotate(String textURL) throws GateException,
            ResourceInstantiationException, MalformedURLException,
            ExecutionException, PersistenceException, IOException {
        /* 1. Initialize GATE library */
        InitApp.initGate();

		/* 2. Load Annie plugin */
        InitApp.loadAnnie();

        /* 3. Initialize WordNet */
        InitApp.initWordNet();

		/* 4. Initialize ANNIE */
        annieController = InitApp.initAnnie();

		/* 6. Document Reset */
        features.clear();
        annieController.add(DocumentReset.PR(features));

		/* 7. Determine Language */
        features.clear();
        annieController.add(LanguageIdentifier.PR(features));


			/* 8. Tokenizer */
        features.clear();
        annieController.add(Tokenizer.PR(features));

			/* 9. Sentences splitter */
        features.clear();
        annieController.add(SentenceSplitter.PR(features));

			/* 10. Speech tagger */
        features.clear();
        annieController.add(POSTagger.PR(features));

			/* 11. Morpher */
        features.clear();
        annieController.add(Morpher.PR(features));

			/* 12. Gazetteer */
        features.clear();
        annieController.add(DefaultGazetteer.PR(features));
        features.put("listPR", annieController.getPRs());
        annieController.add(Gazetteer.PR(features));

			/* 13. Transducer, Adding JAPE rules */
        features.clear();
        features.put("grammarURL", "resources/NE/main.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/skills.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/do_not.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/would_like_to.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/will.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/negative_modal.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/edu_org.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/degree.jape");
        annieController.add(Transducer.PR(features));

        features.clear();
        features.put("grammarURL", "jape_rules/education_via_edu_org.jape");
        annieController.add(Transducer.PR(features));

			/* 14. Alias matcher */
        features.clear();
        annieController.add(AliasMatcher.PR(features));

			/* 15. Pronominal Coreference */
        features.clear();
        annieController.add(PronominalCoreference.PR(features));

			/* 16. Testing that all PRs have the required parameters to run */
        System.out.println("Testing PRs");
        annieController.getOffendingPocessingResources();

        /* 5 Set Corpus */
        File f = new File(textURL);
        corpus = InitApp.initCorpus(f);
        annieController.setCorpus(corpus);


			/* 17. Executing ANNIE */
        features.clear();
        annieController.execute();

        InitApp.printResults();

            /* 17. methods for finding definitions and adding new skills */
//        InitApp.findDefinitions("Java");
//        InitApp.addSkill("Java", "programming_languages");

    }
}

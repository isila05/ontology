package ontology.service.annie;

import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.ProcessingResource;
import gate.util.GateException;
import gate.util.Out;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.MalformedURLException;

/**
 * @author Igor Sila
 */
public class Morpher {
    private static final Logger LOGGER = LoggerFactory.getLogger(Morpher.class);
    static ProcessingResource morpher;
    public static ProcessingResource PR(FeatureMap features) {
        try {
            // Set plugin repo
            File pluginsHome = Gate.getPluginsHome();

            try {
                // Loading all the Plugins (ANNIE, Tools, Ontology,
                // Gazetteer_Ontology_Based)
                Gate.getCreoleRegister().registerDirectories(
                        new File(pluginsHome, "Tools").toURL());
            } catch (MalformedURLException | GateException ex) {
                Out.prln(ex);
                LOGGER.error(ex.toString());
            }
            //Define regExSentenceSplitter Resource by calling the right plugin
            morpher = (ProcessingResource)
                    Factory.createResource("gate.creole.morph.Morph", features);
        } catch (Exception e) {
            Out.prln(e);
            LOGGER.error(e.toString());
        }
        return morpher;
    }
}

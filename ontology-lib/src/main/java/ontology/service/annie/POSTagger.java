package ontology.service.annie;

import gate.Factory;
import gate.FeatureMap;
import gate.ProcessingResource;
import gate.util.Out;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Igor Sila
 */

public class POSTagger{
    private static final Logger LOGGER = LoggerFactory.getLogger(POSTagger.class);
    static ProcessingResource POSTagger;
    public static ProcessingResource PR(FeatureMap features) {
        try {
            //Define POSTagger Resource by calling the right plugin
            POSTagger = (ProcessingResource)
                    Factory.createResource("gate.creole.POSTagger", features);
        } catch (Exception e) {
            Out.prln(e);
            LOGGER.error(e.toString());
        }
        return POSTagger;
    }
}

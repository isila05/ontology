package ontology.service.annie;

import gate.Factory;
import gate.FeatureMap;
import gate.ProcessingResource;
import gate.creole.orthomatcher.OrthoMatcher;
import gate.util.Out;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Igor Sila
 */
public class AliasMatcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(AliasMatcher.class);
    public static OrthoMatcher orthoMatcher;
    private static ProcessingResource aliasMatcher;
    public static ProcessingResource PR(FeatureMap features) {
        try {
            //Define Alias Matcher Resource by calling the right plugin
            aliasMatcher = (ProcessingResource)
                    Factory.createResource("gate.creole.orthomatcher.OrthoMatcher", features);
            orthoMatcher =(OrthoMatcher) aliasMatcher;
        } catch (Exception e) {
            Out.prln(e);
            LOGGER.error(e.toString());
        }
        return aliasMatcher;
    }
}

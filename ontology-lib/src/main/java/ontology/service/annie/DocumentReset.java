package ontology.service.annie;

import gate.Factory;
import gate.FeatureMap;
import gate.ProcessingResource;
import gate.creole.ResourceInstantiationException;
import gate.util.Out;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Igor Sila
 */
public class DocumentReset {
    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentReset.class);
    static ProcessingResource documentReset;
    public static ProcessingResource PR(FeatureMap features) {
        try {
            // Calls the Document Reset that cleans up the document from all previous annotations
            documentReset = (ProcessingResource)
                    Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR", features);
        }
        catch (ResourceInstantiationException ex) {
            Out.prln(ex);
            LOGGER.error(ex.toString());
        }
        return documentReset;
    }
}

package ontology.service.annie;

import gate.Factory;
import gate.FeatureMap;
import gate.ProcessingResource;
import gate.util.Out;
import org.slf4j.LoggerFactory;

/**
 * @author Igor Sila
 */
public class Transducer {
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(Transducer.class);
    static ProcessingResource transducer;
    public static ProcessingResource PR(FeatureMap features) {
        try {
            transducer = (ProcessingResource)
                    Factory.createResource("gate.creole.Transducer", features);
        } catch (Exception e) {
            Out.prln(e);
            LOGGER.error(e.toString());
        }
        return transducer;
    }
}

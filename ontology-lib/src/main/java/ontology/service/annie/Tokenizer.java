package ontology.service.annie;

import gate.Factory;
import gate.FeatureMap;
import gate.ProcessingResource;
import gate.util.Out;
import org.slf4j.LoggerFactory;

/**
 * @author Igor Sila
 */
public class Tokenizer {
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(Tokenizer.class);
    static ProcessingResource tokenizer;
    public static ProcessingResource PR(FeatureMap features) {
        try {
            // Tokeniser cuts out the input text into tokens/entities
            tokenizer = (ProcessingResource)
                    Factory.createResource("gate.creole.tokeniser.DefaultTokeniser", features);
        } catch (Exception e) {
            Out.prln(e);
            LOGGER.error(e.toString());
        }
        return tokenizer;
    }
}

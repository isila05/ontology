package ontology.service.annie;

import gate.Factory;
import gate.FeatureMap;
import gate.ProcessingResource;
import gate.util.Out;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Igor Sila
 */
public class SentenceSplitter {
    private static final Logger LOGGER = LoggerFactory.getLogger(SentenceSplitter.class);
    static ProcessingResource regExSentenceSplitter;
    public static ProcessingResource PR(FeatureMap features) {
        try {
            //Define regExSentenceSplitter Resource by calling the right plugin
            regExSentenceSplitter = (ProcessingResource)
                    Factory.createResource("gate.creole.splitter.RegexSentenceSplitter", features);
        } catch (Exception e) {
            Out.prln(e);
            LOGGER.error(e.toString());
        }
        return regExSentenceSplitter;
    }
}

package ontology.service.annie;

import gate.Factory;
import gate.FeatureMap;
import gate.ProcessingResource;
import gate.util.Out;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Igor Sila
 */
public class DefaultGazetteer {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultGazetteer.class);
    static ProcessingResource AnnieGaz;
    public static ProcessingResource PR(FeatureMap features) {
        try {
            AnnieGaz = (ProcessingResource)
                    Factory.createResource("gate.creole.gazetteer.DefaultGazetteer", features);
        } catch (Exception e) {
            Out.prln(e);
            LOGGER.error(e.toString());
        }
        return AnnieGaz;
    }
}

package ontology.service.annie;

import gate.Factory;
import gate.FeatureMap;
import gate.ProcessingResource;
import gate.creole.ResourceInstantiationException;
import gate.creole.ontology.Ontology;
import gate.util.Out;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.MalformedURLException;
import java.util.List;

/**
 * @author Igor Sila
 */
public class Gazetteer {
    private static final Logger LOGGER = LoggerFactory.getLogger(Gazetteer.class);
    private static final long serialVersionUID = 1L;
    private static Ontology ontology;

    // OntoRootGazetteer is a Gazetteer makes lookup request to Ontology, GIST

    @SuppressWarnings("deprecation")
    public static ProcessingResource PR(FeatureMap features) {

        // Instantiate Processing resources variables
        ProcessingResource ontoRG;
        ProcessingResource flexibleGaz = null;

        // Create parameter for out ontology
        FeatureMap paramOntology = Factory.newFeatureMap();
        try {
            // Set the ontology parameter with our OWL path
            paramOntology.put("rdfXmlURL",
                    new File((String)features.get("ontologyFilePath")).toURL());
            // Create our PR with our previous param
            ontology = (Ontology) Factory.createResource(
                    "gate.creole.ontology.owlim.OWLIMOntologyLR", paramOntology);

        } catch (MalformedURLException | ResourceInstantiationException e) {
            Out.prln(e);
            LOGGER.error(e.toString());
        }

        try {

            // Creation of parameter for onToGaz
            FeatureMap paramsOntoGaz = Factory.newFeatureMap();
//            List<ProcessingResource> listPR = App.annieController.getPRs();
            List<ProcessingResource> listPR = (List<ProcessingResource>)features.get("listPR");
            // Setting the parameters
            paramsOntoGaz.put("ontology", ontology);
            paramsOntoGaz.put("tokeniser", listPR.stream().filter(pr -> pr.getName().toLowerCase().contains("tokeniser")).findFirst().get());
            paramsOntoGaz.put("posTagger", listPR.stream().filter(pr -> pr.getName().toLowerCase().contains("pos") && pr.getName().toLowerCase().contains("tagger")).findFirst().get());
            paramsOntoGaz.put("morpher", listPR.stream().filter(pr -> pr.getName().toLowerCase().contains("morph")).findFirst().get());
            // Creation of the ontoRootGazetteer PR with previous params
            ontoRG = (ProcessingResource) Factory.createResource(
                    "gate.clone.ql.OntoRootGaz", paramsOntoGaz);
            ontoRG.init();

            // Creation of parameter for FlexibleGaz
            FeatureMap paramsFlexibleGaz = Factory.newFeatureMap();
            // Setting the parameters
            paramsFlexibleGaz.put("gazetteerInst", ontoRG);
            paramsFlexibleGaz.put("inputFeatureNames", "Token.root");
            // Creation of the flexibleGazetteer PR with previous params
            flexibleGaz = (ProcessingResource) Factory.createResource(
                    "gate.creole.gazetteer.FlexibleGazetteer",
                    paramsFlexibleGaz);


        } catch (ResourceInstantiationException e) {
            Out.prln(e);
            LOGGER.error(e.toString());
        }
        return flexibleGaz;

    }
}

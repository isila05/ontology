package ontology.service;

import com.hp.hpl.jena.rdf.model.*;
import com.hp.hpl.jena.util.FileManager;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OntologyManager {
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(OntologyManager.class);
    private static volatile OntologyManager instance;
    static final String ONTOLOGY_FILE_PATH = String.format("%sskills.owl", ServiceHelper.RESOURCE_PATH);
    private Model model;

    private OntologyManager() {
        model = ModelFactory.createDefaultModel();
    }

    public static OntologyManager getInstance() {
        if (instance == null) {
            synchronized (OntologyManager.class) {
                if (instance == null) {
                    instance = new OntologyManager();
                }
            }
        }
        return instance;
    }

    public synchronized Map<String, Set<String>> findParent(Set<String> skills) {
        try (InputStream in = FileManager.get().open(ONTOLOGY_FILE_PATH)) {
            if (in == null) {
                throw new IllegalArgumentException("File: not found");
            }
            model.read(in, null);
            Map<String, String> map = getSuperClasses(model);
            Set<String> parents = new HashSet<>();
            Map<String, Set<String>> skillsWithParent = new HashMap<>();
            for (String skill : skills) {
                String parent = map.get(skill);
                parents.add(parent);
            }

            for (String parent : parents) {
                Set<String> subSkills = new HashSet<>();
                for (String skill : skills) {
                    if (parent.equals(map.get(skill))) {
                        subSkills.add(skill);
                    }
                }
                skillsWithParent.put(parent, subSkills);
            }
            return skillsWithParent;
        } catch (IOException e) {
            LOGGER.error(e.toString());
        }

        return null;
    }
    public synchronized Map<String, String> findAllSkillParent() {
        try (InputStream in = FileManager.get().open(ONTOLOGY_FILE_PATH)) {
            if (in == null) {
                throw new IllegalArgumentException("File: not found");
            }
            model.read(in, null);
            Map<String, String> map = getSuperClasses(model);
            return map;
        } catch (IOException e) {
            LOGGER.error(e.toString());
        }

        return null;
    }

    public synchronized Map<String, String> findSkillParent(String skill) {
        try (InputStream in = FileManager.get().open(ONTOLOGY_FILE_PATH)) {
            if (in == null) {
                throw new IllegalArgumentException("File: not found");
            }
            model.read(in, null);
            Map<String, String> map = getSuperClasses(model);
            Map<String, String> skillsWithParent = new HashMap<>();
                String parent = map.get(skill);
            skillsWithParent.put(skill, parent);
            return skillsWithParent;
        } catch (IOException e) {
            LOGGER.error(e.toString());
        }

        return null;
    }

    private Map<String, String> getSuperClasses(Model model) {
        Map<String, String> map = new TreeMap<>();
        StmtIterator iter = model.listStatements();
        // print out the predicate, subject and object of each statement
        while (iter.hasNext()) {
            Statement stmt = iter.nextStatement(); // get next statement
            com.hp.hpl.jena.rdf.model.Resource subject = stmt.getSubject(); // get the subject
            Property predicate = stmt.getPredicate(); // get the predicate
            RDFNode object = stmt.getObject(); // get the object
            object.getClass().getSuperclass();
            if (predicate.toString().contains("subClassOf")) {
                String concept = getConceptName(subject.toString());
                String parent = getConceptName(object.toString());
                map.put(concept, parent);
            }
        }
        return map;
    }

    private String getConceptName(String concept) {
        String regex = "#.*";
        Matcher m;
        Pattern pattern = Pattern.compile(regex);
        m = pattern.matcher(concept);
        if (m.find())
            return m.group(0).substring(1, m.group(0).length());
        return null;
    }

    public synchronized boolean addSkill(String skill, String parent) throws FileNotFoundException {
        InputStream in = FileManager.get().open(ONTOLOGY_FILE_PATH);
        if (in == null) {
            throw new IllegalArgumentException("File: not found");
        }
        Statement s = null;
        Resource r = null;
        boolean once = false;
        boolean written = false;
        String subEntry = "http://www.semanticweb.org/ontologies/skills-ontology#";
        String type = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
        model.read(in, null);
        StmtIterator iter = model.listStatements();
        while (iter.hasNext()) {
            Statement stmt = iter.nextStatement(); // get next statement
            Property predicate = stmt.getPredicate(); // get the predicate
            RDFNode object = stmt.getObject(); // get the object
            object.getClass().getSuperclass();

            if (object.toString().equals(subEntry + parent)) {
                r = model.createResource(subEntry + skill);
                s = model.createStatement(r, predicate, object);
                model.add(s);
            } else if (s != null && predicate.toString().equals(type) && !once) {
                r.addProperty(predicate, object);
                once = true;

                File f = new File(ONTOLOGY_FILE_PATH);
                FileOutputStream outputStream = new FileOutputStream(f);
                model.write(outputStream, null);
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                written = true;
            }
        }
        return written;
    }
    public synchronized void addSkillWithParent(String skill, String parent, String head) throws FileNotFoundException {
        InputStream in = FileManager.get().open(ONTOLOGY_FILE_PATH);
        if (in == null) {
            throw new IllegalArgumentException("File: not found");
        }
        Statement s = null;
        Resource r = null;
        Statement s1 = null;
        Resource r1 = null;
        boolean once = false;
        String subEntry = "http://www.semanticweb.org/ontologies/skills-ontology#";
        String type = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
        model.read(in, null);
        StmtIterator iter = model.listStatements();
        while (iter.hasNext()) {
            Statement stmt = iter.nextStatement(); // get next statement
            Property predicate = stmt.getPredicate(); // get the predicate
            RDFNode object = stmt.getObject(); // get the object
            object.getClass().getSuperclass();

            if (object.toString().equals(subEntry + head) && !once) {
                r = model.createResource(subEntry + parent);
                s = model.createStatement(r, predicate, object);
                model.add(s);
                r1 =  model.createResource(subEntry + skill);
                s1 =  model.createStatement(r1, predicate, r.toString());
                model.add(s1);
            } else if (s != null && s1!= null && predicate.toString().equals(type) && !once) {
                r.addProperty(predicate, object);
                r1.addProperty(predicate, object);
                once = true;
                File f = new File(ONTOLOGY_FILE_PATH);
                FileOutputStream outputStream = new FileOutputStream(f);
                model.write(outputStream, null);
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}

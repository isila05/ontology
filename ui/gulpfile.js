// Инициализируем плагины
var lr = require('tiny-lr'), // web-server for livereload
        gulp = require('gulp'), // Gulp JS
        livereload = require('gulp-livereload'), // Livereload for Gulp
        csso = require('gulp-csso'), // minify CSS
        uglify = require('gulp-uglify'), // minify JS
        concat = require('gulp-concat'), // concat files
        webserver = require('gulp-webserver'),
        minifyCSS = require('gulp-minify-css'),
        imagemin   = require('gulp-imagemin'),
        clean = require('gulp-clean'),
        server = lr(),
        mkdirp = require('mkdirp');



// build css
gulp.task('css', function() {
    gulp.src('./src/**/*.css')
            .pipe(minifyCSS())
            .pipe(gulp.dest('./dist'))
});


// collect JS
gulp.task('js', function() {
      gulp.src('./src/**/*.js')
            .pipe(gulp.dest('./dist'))
});

// collect images
gulp.task('images', function() {
    return gulp.src(['./src/**/*.jpg','./src/**/*.jpeg','./src/**/*.gif','./src/**/*.png'])
            .pipe(imagemin())
            .pipe(gulp.dest('./dist'))
});

gulp.task('clean', function () {
    gulp.src('./dist')
            .pipe(clean());
});

gulp.task('html', function () {
    gulp.src('./src/**/*.html')
            .pipe(gulp.dest('./dist'))
            .pipe(livereload(server));
});

// local server for developing
gulp.task('server', function() {
    gulp.src('./src')
            .pipe(webserver({
                port:       9000,
                livereload: true,
                proxies: [
                    {
                        source: '/ontology/api',
                        target: 'http://localhost:8080/api'
                    }
                ]
            }));
});

// start developer server gulp watch
gulp.task('watch', function() {
    // Предварительная сборка проекта
    gulp.run('js');
    gulp.run('css');
    gulp.run('html');

    // add Livereload
    server.listen(35729, function(err) {
        if (err) return console.log(err);

        gulp.watch('./src/**/*.css', function() {
            gulp.run('css');
        });
        gulp.watch('./src/**/*.js', function() {
            gulp.run('js');
        });
        gulp.watch('./src/**/*.html', ['html']);
    });
    gulp.run('server');
});

gulp.task('default', ['css', 'js', 'html', 'images' ]);
gulp.task('build', ['clean', 'default']);
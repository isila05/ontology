'use strict';

angular.module('statelessApp').controller('ForgotPasswordController', function ($scope, TokenStorage, UserService, $location, $http) {
    $scope.isValidInstall = true;

    $scope.change = function () {
        if($scope.password !== undefined && $scope.repeatpassword !== undefined  && $scope.username !== undefined  && $scope.email !== undefined){
            if($scope.password == $scope.repeatpassword){
                $http.post('ontology/api/forgotPass', { username: $scope.username, email: $scope.email, password: $scope.password}).success(function () {
                    $location.path('/login');
                    $scope.isValidInstall = true;
                }).error(function(result, status, headers){
                    if(status == 400){
                        $scope.isValidInstall = false;
                        $scope.modelDescription = 'Wrong username or email';
                    }
                    if(status == 404){
                        $scope.isValidInstall = false;
                        $scope.modelDescription = 'User does not exist';
                    }
                    $scope.authenticated = false;

                })
            }
            else{
                $scope.isValidInstall = false;
                $scope.modelDescription = 'Password and repeat password must be the same';
            }
        }
    };

    $scope.back = function () {
        $location.path('/login');
    };
});

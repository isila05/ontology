'use strict';

angular.module('statelessApp').controller('RegisterController', function ($scope, $http, TokenStorage, $location, UserService) {
    $scope.isValidInstall = true;

    $scope.register = function () {
        var fullname =  $scope.firstname + ' ' + $scope.lastname;
        if($scope.username !== undefined && $scope.email !== undefined && $scope.password !== undefined &&
                $scope.address !== undefined && $scope.phone !== undefined &&
                fullname !== undefined && $scope.repeatpassword !== undefined && $scope.dob !== undefined ){
            if($scope.password == $scope.repeatpassword){
                $http.post('ontology/api/create', { username: $scope.username, email:$scope.email, password: $scope.password,
                    address: $scope.address, phone: $scope.phone, fullname: fullname, dateOfBirth: $scope.dob}).success(function (result, status, headers) {
                        $http.post('ontology/api/login', { username: $scope.username, password: $scope.password }).success(function (result, status, headers) {
                            $scope.authenticated = true;
                            TokenStorage.store(headers('X-AUTH-TOKEN'));

                            // For display purposes only
                            $scope.token = JSON.parse(atob(TokenStorage.retrieve().split('.')[0]));

                            UserService.setUser({username: $scope.username, password: $scope.password, authenticated: $scope.authenticated, token: $scope.token});
                            $scope.isValidInstall = true;
                            $location.path('/home');
                        })
                                .error(function(result, status, headers){
                                    $scope.authenticated = false;

                                })
                }).error(function(result, status, headers){
                    if(status == 400){
                        $scope.isValidInstall = false;
                        $scope.modelDescription = 'User with this username or email already exists, choose another one';
                        return;
                    }

                })
            }
            else{
                $scope.isValidInstall = false;
                $scope.modelDescription = 'Password and repeat password must be the same';
            }
        }
    };

    $scope.back = function () {
        $location.path('/login');
    };
});

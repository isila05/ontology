'use strict';

angular.module('statelessApp').controller('HomeController', function ($scope,$rootScope, $http, TokenStorage, $location, UserService, CVService) {
    $scope.sv = CVService.getCV();
    $scope.skillsReturned = $scope.sv.autobiography !== undefined;
    $scope.autobiographyPlacoholder = $scope.sv.autobiography !== undefined ? $scope.sv.autobiography : '';
    $scope.skillsPlacoholder = $scope.sv.skills !== undefined ? $scope.sv.skills : '';
    $scope.educationPlacoholder = $scope.sv.education !== undefined ? $scope.sv.education : '';
    $scope.addinfoPlacoholder = $scope.sv.addinfo !== undefined ? $scope.sv.addinfo : '';

    $scope.autobiography = $scope.autobiographyPlacoholder;
    $scope.skills = $scope.skillsPlacoholder;
    $scope.education = $scope.educationPlacoholder;
    $scope.addinfo = $scope.addinfoPlacoholder;

    $scope.skillparent = false;
    $scope.parents = [{}, {}];
    $scope.selectedOption = $scope.parents[0];

    $scope.logout = function () {
        TokenStorage.clear();
        $scope.authenticated = false;
        $location.path('/login');
        UserService.setUser({username: '', password: '', authenticated: '', token: ''});
        CVService.setCV({skills: '', education: '', addinfo: '', autobiography: ''});
        UserService.clearUser();
        CVService.clearCV();
    };

    $scope.receiveProcessedBio = function () {
        $scope.resume = null;
        $http.post('ontology/api/onto/processText', {biography: $scope.autobiography, username: UserService.getUser().username}).then(function (responce) {
            CVService.setCV({skills: responce.data.skills, education: responce.data.educations,
                addinfo: $scope.addinfo, autobiography: $scope.autobiography, possibleSkills: responce.data.possibleSkills});
            $scope.resume = CVService.getCV();

        });
    };

    $scope.changeInfo = function () {
        if($scope.resume !== undefined){
            CVService.setCV({skills: $scope.resume.skills, education: $scope.resume.education,
                addinfo: $scope.addinfo, autobiography: $scope.autobiography, possibleSkills: $scope.resume.possibleSkills});
        }
        $location.path('/changeInfo');
    };
    $scope.addSkill = function () {
        if($scope.skillparent){

           if($scope.newSkill !== undefined && $scope.textParent !== undefined && $scope.textParent !== null){

               var tempCategory = $scope.textParent.replace(/_/g, ' ');
               var newTempCategory = tempCategory.charAt(0).toUpperCase() + tempCategory.slice(1);
               if($scope.resume.skills[newTempCategory] === undefined){
                   if($scope.resume.skills[tempCategory] === undefined){
                       $scope.resume.skills[newTempCategory] = [$scope.newSkill];
                   }
                   else{
                       $scope.resume.skills[tempCategory].push($scope.newSkill);
                   }
               }
               else{
                   $scope.resume.skills[tempCategory].push($scope.newSkill);
               }
               tempCategory = tempCategory.charAt(0).toLowerCase() + tempCategory.slice(1);
               tempCategory = tempCategory.replace(' ', '_');
               $http.post('ontology/api/onto/addNewSkill', {skill: $scope.newSkill, parent: tempCategory, createParent: true,
                   username: UserService.getUser().username}).then(function (responce) {
                   $scope.resume = null;
                   $scope.newSkill = undefined;
                   $scope.textParent = undefined;
                   $scope.parents = [{}, {}];
                   $scope.skillparent = false;
                   $scope.createParent = false;
                   CVService.clearCV();
                   CVService.setCV({skills: responce.data.skills, education: responce.data.educations,
                       addinfo: $scope.addinfo, autobiography: $scope.autobiography, possibleSkills: responce.data.possibleSkills});
                   $scope.resume = CVService.getCV();

               });
           }
            else if($scope.newSkill !== undefined && $scope.selectedOption !== undefined && $scope.selectedOption !== null) {
               var tempCategory = $scope.selectedOption.replace(/_/g, ' ');
               var newTempCategory = tempCategory.charAt(0).toUpperCase() + tempCategory.slice(1);
               if($scope.resume.skills[newTempCategory] === undefined){
                   if($scope.resume.skills[tempCategory] === undefined){
                       $scope.resume.skills[newTempCategory] = [$scope.newSkill];
                   }
                   else{
                       $scope.resume.skills[tempCategory].push($scope.newSkill);
                   }
               }
               else{
                   $scope.resume.skills[tempCategory].push($scope.newSkill);
               }
               $http.post('ontology/api/onto/addNewSkill', {skill: $scope.newSkill, parent: $scope.selectedOption,createParent: false,
                   username: UserService.getUser().username}).then(function (responce) {
                   $scope.resume = null;
                   $scope.newSkill = undefined;
                   $scope.selectedOption = undefined;
                   $scope.textParent = undefined;
                   $scope.parents = [{}, {}];
                   $scope.skillparent = false;
                   CVService.clearCV();
                   CVService.setCV({skills: responce.data.skills, education: responce.data.educations,
                       addinfo: $scope.addinfo, autobiography: $scope.autobiography, possibleSkills: responce.data.possibleSkills});
                   $scope.resume = CVService.getCV();

               });
           }
        }
        else{
            if($scope.newSkill !== undefined){
                $http.post('ontology/api/onto/findParentSkill', {skill: $scope.newSkill, username: UserService.getUser().username}).then(function (responce) {
                    $scope.parents =  responce.data;
                    $scope.skillparent = true;
                    if($scope.parents.length >0){
                        $scope.inputparent = false;
                    }
                     else{
                        $scope.inputparent = true;
                    }
                });
            }
        }
    };

    $scope.viewCV = function () {
        CVService.clearCV();
        CVService.setCV({skills: $scope.resume.skills, education: $scope.resume.education,
            addinfo: $scope.addinfo, autobiography: $scope.autobiography, possibleSkills: $scope.resume.possibleSkills});
        $location.path('/viewCV');
    };
    $scope.changeParentView = function () {
        $scope.inputparent = !$scope.inputparent;
    };
});

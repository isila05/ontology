'use strict';

angular.module('statelessApp').controller('HeaderController', function ($scope, TokenStorage, UserService, $location) {
    $scope.locate = function () {
        TokenStorage.clear();
        $location.path('/login');
        UserService.clearUser();
    };
});

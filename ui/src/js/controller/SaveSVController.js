'use strict';

angular.module('statelessApp').controller('SaveSVController', function ($scope, $http, TokenStorage, UserService, CVService, $location, $filter) {
    $scope.sv = CVService.getCV();
    $scope.info = $scope.sv.addinfo !== undefined && $scope.sv.addinfo.length  > 0;

    $http.post('ontology/api/user/getInfo', {username: UserService.getUser().username }).then(function (responce) {
        $scope.fullname = responce.data.fullname;
        $scope.email = responce.data.email;
        $scope.phone = responce.data.phone;
        $scope.address = responce.data.address;
        $scope.dob =  new Date(responce.data.dateOfBirth);
    });

    $scope.back = function () {
        $location.path('/home');
    };

    $scope.downloadPdf = function () {
        var doc = new jsPDF();
        var padding = 10;
        doc.text(100, padding, 'Resume');
        padding+=10;
        doc.text(20, padding, 'Full name: ' + $scope.fullname);
        padding+=10;
        doc.text(20, padding, 'Email: ' + $scope.email);
        padding+=10;
        doc.text(20, padding, 'Phone: ' + $scope.phone);
        padding+=10;
        doc.text(20, padding, 'Address: ' + $scope.address);
        padding+=10;
        var date = $filter('date')(new Date($scope.dob), 'MM/dd/yyyy');
        doc.text(20, padding, 'Date Of Birth: ' +  date);
        padding+=10;
        doc.text(20, padding, 'Education: ');
        padding+=7;
        for(var category in $scope.sv.education){
            if($scope.sv.education[category].length > 0){
                                 if(padding+ 7*$scope.sv.education[category].length>=280){
                    doc.addPage();
                    padding = 10;
                }
                doc.text(25, padding, category);
                for(var element = 0; element <$scope.sv.education[category].length;  element++){
                    padding+=7;
                    doc.text(30, padding, $scope.sv.education[category][element]);
                }
                padding+=10;
            }
            }
        if(padding>=290){
            doc.addPage();
        }
        doc.text(20, padding, 'Skills: ');
        padding+=7;
        for(var category in $scope.sv.skills){
            if($scope.sv.skills[category].length > 0){
                if(padding+ 7*$scope.sv.skills[category].length>=280){
                    doc.addPage();
                    padding = 10;
                }
                var tempCategory = category.charAt(0).toUpperCase() + category.slice(1);
                tempCategory = category.replace(/_/g, ' ');
                tempCategory = tempCategory.charAt(0).toUpperCase() + tempCategory.slice(1);
                doc.text(25, padding, tempCategory);
                for(var skillselement = 0; skillselement <$scope.sv.skills[category].length;  skillselement++){
                    padding+=7;
                    element = $scope.sv.skills[category][skillselement].charAt(0).toUpperCase() + $scope.sv.skills[category][skillselement].slice(1);
                    element = element.replace(/_/g, ' ');
                    element.charAt(0).toUpperCase();

                    doc.text(30, padding, element);
                }
                if(padding>=280){
                    doc.addPage();
                    padding = 10;
                }
                padding+=10;
            }
            }

        if($scope.info){
            var sentenses = $scope.sv.addinfo.replace(/([.?!])\s*(?=[A-Z])/g, "$1|").split("|");
            if(padding+ 6*sentenses.length>=280){
                doc.addPage();
                padding = 10;
            }
            doc.text(20, padding, 'Additional Info: ');
            for(var i in sentenses){
                padding+=6;
                doc.text(20, padding, sentenses[i]);
            }

        }

        doc.save('cv.pdf');
        $http.post('ontology/api/onto/saveCV', {skills: $scope.sv.skills, educations: $scope.sv.education, username: UserService.getUser().username,
        addinfo: $scope.sv.addinfo});
    };

});

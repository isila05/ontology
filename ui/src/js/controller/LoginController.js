'use strict';

angular.module('statelessApp').controller('LoginController', function ($scope, $http, TokenStorage, $location, UserService) {
    $scope.authenticated = false;
    $scope.token; // For display purposes only
    $scope.isValidInstall = true;

    $scope.register = function () {
        $location.path('/register');
    };
    $scope.forgot = function () {
        $location.path('/forgot');
    };

    $scope.login = function () {
        if($scope.username !== undefined && $scope.password !== undefined){
            $http.post('ontology/api/login', { username: $scope.username, password: $scope.password }).success(function (result, status, headers) {
                $scope.authenticated = true;
                TokenStorage.store(headers('X-AUTH-TOKEN'));

                // For display purposes only
                $scope.token = JSON.parse(atob(TokenStorage.retrieve().split('.')[0]));

                UserService.setUser({username: $scope.username, password: $scope.password, authenticated: $scope.authenticated, token: $scope.token});
                $scope.isValidInstall = true;
                $location.path('/home');
            })
                    .error(function(result, status, headers){
                        if(status == 401){
                            $scope.isValidInstall = false;
                            $scope.modelDescription = 'Wrong username or password';
                        }
                        $scope.authenticated = false;

                    })
        }
    };
});

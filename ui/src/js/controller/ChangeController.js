'use strict';

angular.module('statelessApp').controller('ChangeController', function ($scope, TokenStorage, UserService, $location, $http) {
    $http.post('ontology/api/user/getInfo', {username: UserService.getUser().username }).then(function (responce) {
        $scope.fullname = responce.data.fullname;
        $scope.email = responce.data.email;
        $scope.phone = responce.data.phone;
        $scope.address = responce.data.address;
        $scope.dob = new Date(responce.data.dateOfBirth);
    });


    $scope.back = function () {
        $location.path('/home');
    };
    $scope.changeInfo = function () {
        if($scope.fullname !== undefined && $scope.email !== undefined && $scope.address !== undefined && $scope.phone !== undefined && $scope.dob !== undefined){
            $http.post('ontology/api/user/change', {username: UserService.getUser().username, email:$scope.email,
                address: $scope.address, phone: $scope.phone, fullname: $scope.fullname, dateOfBirth: $scope.dob}).success(function () {
                $location.path('/home');
            });
        }
    }
});

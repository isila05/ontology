'use strict';
angular.module('statelessApp').directive('ontoDisplayCategories', function () {
    return{
        restrict: 'E',
        scope: {
            categories: '=',
            hideButtons: '@'
        },
        template: '<div class="category" ng-repeat="(key, category) in categories">\n    <div ng-show="category.length > 0">\n        <onto-show-category-name category-name="key"/>\n    </div>\n    <div ng-repeat="item in category">\n        <onto-display-resume-element display-value="item" hide-buttons="hideButtons"/>\n    </div>\n</div>'
    }
});

angular.module('statelessApp').directive('ontoDisplayResumeElement', function (CVService, $http) {
    return{
        restrict: 'EA',
        scope: {
            displayValue: '=',
            hideButtons: '='
        },
        template: '<div class="element">\n    <div ng-hide="isEditMode">\n        <span>{{displayValue}}</span>\n        <button ng-hide="hideButtons" ng-click="editElement()">Edit</button>\n        <button ng-hide="hideButtons" ng-click="deleteElement()">Delete</button>\n    </div>\n    <div ng-show="isEditMode">\n        <input type="text" ng-model="displayValue">\n        <button ng-hide="hideButtons" ng-click="ok()">Ok</button>\n    </div>\n</div>',
        controller: function ($scope) {
            $scope.cv = CVService.getCV();
            $scope.isEditMode = false;
            $scope.deleteElement = function () {
                if ($scope.$parent.item) {
                    $scope.$parent.category.splice($scope.$parent.$index, 1);
                    $http.post('ontology/api/onto/deleteSkill', {skill: $scope.$parent.item});
                }
            };

            $scope.editElement = function () {
                $scope.isEditMode = true;
            };

            $scope.ok = function(){
                $http.post('ontology/api/onto/editSkill', {skill: $scope.$parent.category[$scope.$parent.$index], parent: $scope.displayValue});
                $scope.$parent.category[$scope.$parent.$index] = $scope.displayValue;
                $scope.isEditMode = false;
            };

        }
    }
});

angular.module('statelessApp').directive('ontoShowCategoryName', function () {
    return{
        restrict: 'EA',
        scope: {
            categoryName: '='
        },
        template:'{{categoryName}}',
        controller: function($scope){
            $scope.categoryName = $scope.categoryName.charAt(0).toUpperCase() + $scope.categoryName.slice(1);
            $scope.categoryName = $scope.categoryName.replace(/_/g, ' ');
        }
    }
});

angular.module('statelessApp').directive('ontoShowSkillName', function ($http, CVService, UserService) {
    return{
        restrict: 'EA',
        scope: {
            displayValue: '=',
            hideButtons: '='
        },
        template:'<div class="element">\n    <div ng-show="displayValue.length > 0">\n        <div>\n            <span>{{displayValue}}</span>\n            <button ng-hide="hideButtons" ng-click="delete()">Delete</button>\n            <button ng-show="showButton()" ng-click="changeView()">Write Parent</button>\n            <button ng-hide="hideButtons" ng-click="add()">Add</button>\n            <div ng-show="$parent.category.length > 0 && !inputparent">\n                <select data-ng-options="o for o in $parent.category"\n                        data-ng-model="selectedOption"></select>\n            </div>\n        </div>\n        <div ng-show="$parent.category.length > 0 && inputparent">\n            <input type="text" placeholder="Input your parent" ng-model="textParent">\n        </div>\n        <div ng-show="!$parent.category.length > 0">\n            <input type="text" placeholder="Input your parent" ng-model="textParent">\n        </div>\n    </div>\n</div>\n',
        controller: function($scope){
            $scope.inputparent = false;
            $scope.resume = CVService.getCV();
            $scope.showButton = function () {
                return $scope.$parent.category.length > 0;
            };

            $scope.add = function () {
                if($scope.textParent !== undefined && $scope.textParent !== null){
                    var tempCategory = $scope.textParent.replace(/_/g, ' ');
                    var newTempCategory = tempCategory.charAt(0).toUpperCase() + tempCategory.slice(1);
                    if($scope.resume.skills[newTempCategory] === undefined){
                        if($scope.resume.skills[tempCategory] === undefined){
                            $scope.resume.skills[newTempCategory] = [$scope.displayValue];
                        }
                        else{
                            $scope.resume.skills[tempCategory].push($scope.displayValue);
                        }
                    }
                    else{
                        $scope.resume.skills[newTempCategory].push($scope.displayValue);
                    }
                    tempCategory = tempCategory.charAt(0).toLowerCase() + tempCategory.slice(1);
                    tempCategory = tempCategory.replace(' ', '_');
                    $http.post('ontology/api/onto/addNewSkill', {skill: $scope.displayValue, parent: tempCategory, createParent: true,
                        username: UserService.getUser().username}).then(function (responce) {
                        $scope.delete();
                        $scope.displayValue = undefined;
                        $scope.selectedOption = undefined;
                        $scope.textParent = undefined;
                        $scope.resume = null;
                        CVService.clearCV();
                        CVService.setCV({skills: responce.data.skills, education: responce.data.educations,
                            addinfo: responce.data.addinfo, autobiography: responce.data.autobiography, possibleSkills: responce.data.possibleSkills});
                        $scope.resume = CVService.getCV();
                    });
                }
                else if($scope.selectedOption !== undefined && $scope.selectedOption !== null) {
                    var tempCategory = $scope.selectedOption.replace(/_/g, ' ');
                    var newTempCategory = tempCategory.charAt(0).toUpperCase() + tempCategory.slice(1);
                    if($scope.resume.skills[newTempCategory] === undefined){
                        if($scope.resume.skills[tempCategory] === undefined){
                            $scope.resume.skills[newTempCategory] = [$scope.displayValue];
                        }
                        else{
                            $scope.resume.skills[tempCategory].push($scope.displayValue);
                        }
                    }
                    else{
                        $scope.resume.skills[newTempCategory].push($scope.displayValue);
                    }
                    $http.post('ontology/api/onto/addNewSkill', {skill: $scope.displayValue, parent: $scope.selectedOption,createParent: false,
                        username: UserService.getUser().username}).then(function (response) {
                        $scope.delete();
                        $scope.displayValue = undefined;
                        $scope.selectedOption = undefined;
                        $scope.textParent = undefined;
                        $scope.resume = null;
                        CVService.clearCV();
                        CVService.setCV({skills: response.data.skills, education: response.data.educations,
                            addinfo: response.data.addinfo, autobiography: response.data.autobiography, possibleSkills: response.data.possibleSkills});
                        $scope.resume = CVService.getCV();
                    });
                }
            };

            $scope.delete = function () {
                if ($scope.$parent.key) {
                    $scope.$parent.category.splice(0, $scope.$parent.category.length);
                    $scope.$parent.key = "";

                }
            };
            $scope.changeView = function () {
                $scope.inputparent = !$scope.inputparent;
            };
        }
    }
});
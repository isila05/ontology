'use strict';
angular.module('statelessApp').directive('ontoDisplayPossibleSkills', function () {
    return{
        restrict: 'E',
        scope: {
            categories: '=',
            hideButtons: '@'
        },
        template: '<div class="category" ng-repeat="(key, category) in categories">\n        <onto-show-skill-name display-value="key" hide-buttons="hideButtons"/>\n</div>'
    }
});
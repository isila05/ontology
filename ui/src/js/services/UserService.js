'use strict';

angular.module('statelessApp').factory('UserService', function(){
    var user = JSON.parse(localStorage.getItem('user'))||{};

    var setUser = function(aUser){
        user.username = aUser.username;
        user.password = aUser.password;
        user.authenticated = aUser.authenticated;
        user.token = aUser.token;
        localStorage.setItem('user',JSON.stringify(user));
    };

    var getUser = function(){
        return user;
    };

    var clearUser = function(){
        localStorage.removeItem('user') ;
    };

    return {
        clearUser: clearUser,
        setUser: setUser,
        getUser: getUser
    }
});

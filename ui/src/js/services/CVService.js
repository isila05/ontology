'use strict';

angular.module('statelessApp').factory('CVService', function(){
    var cv = JSON.parse(localStorage.getItem('cv'))||{};

    var setCV = function(aCV){
        cv.skills = aCV.skills;
        cv.education = aCV.education;
        cv.addinfo = aCV.addinfo;
        cv.autobiography = aCV.autobiography;
        cv.possibleSkills = aCV.possibleSkills;
        localStorage.setItem('cv',JSON.stringify(cv));
    };

    var getCV = function(){
        return cv;
    };

    var clearCV = function(){
        localStorage.removeItem('cv');
    };

    return {
        clearCV: clearCV,
        setCV: setCV,
        getCV: getCV
    }
});

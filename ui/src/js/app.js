'use strict';
angular.module('statelessApp', ['ngRoute', 'ui.date']).factory('TokenStorage', function () {
    var storageKey = 'auth_token';
    return {
        store: function (token) {
            return localStorage.setItem(storageKey, token);
        },
        retrieve: function () {
            return localStorage.getItem(storageKey);
        },
        clear: function () {
            return localStorage.removeItem(storageKey);
        }
    };
}).factory('TokenAuthInterceptor', function ($q, TokenStorage) {
    return {
        request: function (config) {
            var authToken = TokenStorage.retrieve();
            if (authToken) {
                config.headers['X-AUTH-TOKEN'] = authToken;
            }
            return config;
        },
        responseError: function (error) {
            if (error.status === 401 || error.status === 403) {
                TokenStorage.clear();
            }
            return $q.reject(error);
        }
    };
}).config(function ($httpProvider, $routeProvider) {
    $httpProvider.interceptors.push('TokenAuthInterceptor');
    $routeProvider
            .when('/login', {
                templateUrl: 'pages/login.html'
            })
            .when('/register', {
                templateUrl: 'pages/register.html'
            })
            .when('/home', {
                templateUrl: 'pages/home.html'
            })
            .when('/changeInfo', {
                templateUrl: 'pages/change.html'
            })
            .when('/forgot', {
                templateUrl: 'pages/forgot.html'
            })
            .when('/viewCV', {
                templateUrl: 'pages/viewCV.html'
            })
            .otherwise({redirectTo: '/login'});
}).config(['$compileProvider',
    function ($compileProvider) {
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob):/);
    }]);
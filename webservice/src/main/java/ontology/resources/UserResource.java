package ontology.resources;

import ontology.bindings.UserBinding;
import ontology.domain.User;
import ontology.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Igor Sila
 */
@Component
public class UserResource {
    @Autowired
    private UserService userService;
    private static final Logger LOG = LoggerFactory.getLogger(UserResource.class);

    @GET
    @Path(value = "/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UserBinding getUser(@PathParam("id") long id) {
        User user = userService.find(id);
        UserBinding userBinding = new UserBinding();
        userBinding.setId(user.getId());
        userBinding.setUsername(user.getUsername());
        userBinding.setFullname(user.getFullname());
        userBinding.setEmail(user.getEmail());
        userBinding.setAddress(user.getAddress());
        userBinding.setPhone(user.getPhone());
        return userBinding;
    }

    @POST
    @Path(value = "/getInfo")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UserBinding getUser(UserBinding binding) {
        User user = userService.findUserByUserName(binding.getUsername());
        UserBinding userBinding = new UserBinding();
        userBinding.setId(user.getId());
        userBinding.setUsername(user.getUsername());
        userBinding.setFullname(user.getFullname());
        userBinding.setEmail(user.getEmail());
        userBinding.setAddress(user.getAddress());
        userBinding.setPhone(user.getPhone());
        userBinding.setDateOfBirth(user.getDateOfbirth());
        return userBinding;
    }

    @POST
    @Path(value = "/change")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response changeUserInformation(UserBinding binding) {
        User user = userService.findUserByUserName(binding.getUsername());
        if(binding.getFullname() != null){
            user.setFullname(binding.getFullname());
        }
        if(binding.getEmail() != null){
            user.setEmail(binding.getEmail());
        }
        if(binding.getPhone() != null){
            user.setPhone(binding.getPhone());
        }
        if(binding.getAddress() != null){
            user.setAddress(binding.getAddress());
        }
        if(binding.getDateOfBirth() != null){
            user.setDateOfbirth(binding.getDateOfBirth());
        }
        userService.updateUser(user);
        return Response.ok().build();
    }
}

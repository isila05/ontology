package ontology.resources;

import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import ontology.bindings.OntologyBinding;
import ontology.bindings.ResumeBinding;
import ontology.domain.*;
import ontology.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * @author Igor Sila
 */
@Component
public class OntologyResource {
    @Autowired
    private OntologyService ontologyService;
    @Autowired
    private RatingService ratingService;
    @Autowired
    private UserService userService;
    @Autowired
    private ResumeService resumeService;
    @Autowired
    private SkillService skillService;
    @Autowired
    private EducationService educationService;
    private static final Logger LOG = LoggerFactory.getLogger(OntologyResource.class);

    private ResumeBinding resumeBinding;

    @POST
    @Path(value = "/processText")
    @Consumes(MediaType.APPLICATION_JSON)
    public ResumeBinding processBio(OntologyBinding binding) {
        resumeBinding = null;
        try {
            resumeBinding = ontologyService.processText(binding.getBiography());
            for(String s: resumeBinding.getPossibleSkills().keySet()){
                Set set = sortSet(s, binding.getUsername(), resumeBinding.getPossibleSkills().get(s));
                resumeBinding.getPossibleSkills().put(s, set);
            }
        } catch (ResourceInstantiationException | ExecutionException e) {
            LOG.error(e.toString());
        }
        return resumeBinding;
    }


    @POST
    @Path(value = "/deleteSkill")
    @Consumes(MediaType.APPLICATION_JSON)
    public ResumeBinding deleteSkill(OntologyBinding binding) {
        for(String category: resumeBinding.getSkills().keySet()){
            resumeBinding.getSkills().get(category).remove(binding.getSkill());
        }
        return resumeBinding;
    }

    @POST
    @Path(value = "/editSkill")
    @Consumes(MediaType.APPLICATION_JSON)
    public ResumeBinding editSkill(OntologyBinding binding) {
        for(String category: resumeBinding.getSkills().keySet()){
            if(resumeBinding.getSkills().get(category).remove(binding.getSkill())){
                resumeBinding.getSkills().get(category).add(binding.getParent());
            }
        }
        return resumeBinding;
    }

    @POST
    @Path(value = "/saveCV")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveCV(ResumeBinding binding) {
        User user = userService.findUserByUserName(binding.getUsername());
        Resume resume = new Resume();
        resume.setUser(user);
        resume.setAdditionalInfo(binding.getAddinfo());
        resumeService.save(resume);
        for(String parentEducation: binding.getEducations().keySet()){
             for(String value: binding.getEducations().get(parentEducation)){
                 Education education = new Education();
                 education.setEducationType(parentEducation);
                 education.setResume(resume);
                 education.setValue(value);
                 educationService.save(education);
             }
         }
        for(String parentSkill: binding.getSkills().keySet()){
            for(String value: binding.getSkills().get(parentSkill)){
                Skill skill = new Skill();
                skill.setResume(resume);
                skill.setParent(parentSkill);
                skill.setValue(value);
                skillService.save(skill);
            }
        }
        return Response.ok().build();
    }

    @POST
    @Path(value = "/findParentSkill")
    @Consumes(MediaType.APPLICATION_JSON)
    public Set findParentSkill(OntologyBinding binding) {
        Set set = null;
        String definition = null;
        try {
            definition = ontologyService.executeWordNet(binding.getSkill());
            set = ontologyService.findParents(definition);
        } catch (ResourceInstantiationException | ExecutionException e) {
            LOG.error(e.toString());
        }
        User user = userService.findUserByUserName(binding.getUsername());
        List<Rating> userRating = ratingService.findUserRating(user);
        Set<String> ratingSet = new HashSet<>();
        Collections.sort(userRating, new Comparator<Rating>() {
            @Override
            public int compare(Rating o1, Rating o2) {
                return o1.getCount() > o2.getCount() ? 1: o1.getCount().equals(o2.getCount()) ? 0 : -1 ;
            }
        });
        if (null != set) {
            if (userRating.size() > 0 && set.size() > 0) {
                for (Rating r : userRating) {
                    for(Object obj: set){
                       if(r.getParent().equals(obj.toString())){
                           ratingSet.add(obj.toString());
                       }
                    }
                }
            }
            set.removeAll(ratingSet);
            ratingSet.addAll(set);
        }
        for(Rating r: userRating){
            if(definition.contains(r.getParent())){
                ratingSet.add(r.getParent());
            }
        }
        return ratingSet;
    }

    @POST
    @Path(value = "/addNewSkill")
    @Consumes(MediaType.APPLICATION_JSON)
    public ResumeBinding addNewSkill(OntologyBinding binding) {
        try {
            Set<String> set;
            if (binding.getCreateParent()) {
                ontologyService.getOntologyManager().addSkillWithParent(binding.getSkill(), binding.getParent(), "domain_specific_skills_and_competences");
                set = resumeBinding.getSkills().get(binding.getParent());
                if(null != set){
                    set.add(binding.getSkill());
                    resumeBinding.getSkills().put(binding.getParent(),set);
                }
                else{
                    set = new HashSet<>();
                    set.add(binding.getSkill());
                    resumeBinding.getSkills().put(binding.getParent(),set);
                }
            } else {
                if (!ontologyService.getOntologyManager().addSkill(binding.getSkill(), binding.getParent())) {
                    Map<String, String> map = ontologyService.getOntologyManager().findSkillParent(binding.getParent());
                    ontologyService.getOntologyManager().addSkillWithParent(binding.getSkill(), binding.getParent(), map.get(binding.getParent()));
                    set = resumeBinding.getSkills().get(binding.getParent());
                    if(null != set){
                        set.add(binding.getSkill());
                        resumeBinding.getSkills().put(binding.getParent(),set);
                    }
                    else{
                        set = new HashSet<>();
                        set.add(binding.getSkill());
                        resumeBinding.getSkills().put(binding.getParent(),set);
                    }
                }
                else{
                    set = resumeBinding.getSkills().get(binding.getParent());
                    if(null != set){
                        set.add(binding.getSkill());
                        resumeBinding.getSkills().put(binding.getParent(),set);
                    }
                    else{
                        set = new HashSet<>();
                        set.add(binding.getSkill());
                        resumeBinding.getSkills().put(binding.getParent(),set);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            LOG.error(e.toString());
        }
        User user = userService.findUserByUserName(binding.getUsername());
        List<Rating> userRating = ratingService.findUserRating(user);
        boolean wasFound = false;
        if (userRating.size() > 0) {
            for (Rating r : userRating) {
                if (r.getParent().equals(binding.getParent())) {
                    r.setCount(r.getCount() + 1);
                    wasFound = true;
                    ratingService.save(r);
                }
            }
        }
        if (!wasFound) {
            Rating rating = new Rating();
            rating.setUser(user);
            rating.setParent(binding.getParent());
            rating.setCount(1l);
            ratingService.save(rating);
        }
        resumeBinding.getPossibleSkills().remove(binding.getSkill());
         for(String s: resumeBinding.getPossibleSkills().keySet()){
             Set set = sortSet(s, binding.getUsername(), resumeBinding.getPossibleSkills().get(s));
             resumeBinding.getPossibleSkills().put(s, set);
         }
        return resumeBinding;
    }
    public Set sortSet(String skill, String username, Set<String> possibleParents) {
        Set set = null;
        String definition = null;
        try {
            definition = ontologyService.executeWordNet(skill);
            set = ontologyService.findParents(definition);
            if(possibleParents != null && possibleParents.size() > 0){
                set.addAll(possibleParents);
            }
        } catch (ResourceInstantiationException | ExecutionException e) {
            LOG.error(e.toString());
        }
        User user = userService.findUserByUserName(username);
        List<Rating> userRating = ratingService.findUserRating(user);
        Set<String> ratingSet = new HashSet<>();
        if(userRating.size() > 0){
            Collections.sort(userRating, new Comparator<Rating>() {
                @Override
                public int compare(Rating o1, Rating o2) {
                    return o1.getCount() > o2.getCount() ? 1: o1.getCount().equals(o2.getCount()) ? 0 : -1 ;
                }
            });
            if (null != set) {
                if (userRating.size() > 0 && set.size() > 0) {
                    for (Rating r : userRating) {
                        for(Object obj: set){
                            if(r.getParent().equals(obj.toString())){
                                ratingSet.add(obj.toString());
                            }
                        }
                    }
                }
                set.removeAll(ratingSet);
                ratingSet.addAll(set);
            }
            for(Rating r: userRating){
                if(definition.contains(r.getParent())){
                    ratingSet.add(r.getParent());
                }
            }
        }
        else{
            ratingSet.addAll(set);
            userRating = ratingService.findAll();
            for(Rating r: userRating){
                if(definition.contains(r.getParent())){
                    ratingSet.add(r.getParent());
                }
            }
        }


        return ratingSet;
    }
}

package ontology.resources;

import ontology.bindings.UserBinding;
import ontology.domain.User;
import ontology.exception.UserAlreadyExistsException;
import ontology.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Igor Sila
 */

@Path("api")
@Component
public class ApiResource {
    @Autowired
    private UserResource userResource;
    @Autowired
    private UserService userService;
    @Autowired
    private OntologyResource ontologyResource;

    private static final Logger LOG = LoggerFactory.getLogger(ApiResource.class);

    @Path("user")
    public UserResource getUserResource() {
        return userResource;
    }
    @Path("onto")
    public OntologyResource getOntologyResource() {
        return ontologyResource;
    }

    @POST
    @Path(value = "create")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createUser(UserBinding binding) throws UserAlreadyExistsException {
        User existUser = userService.findUserByEmail(binding.getEmail());
        if(null != existUser) {
            LOG.error("Bad Request! User already exists ");
            return Response.status(400).build();
        }
        existUser = userService.findUserByUserName(binding.getUsername());
        if(null != existUser) {
            LOG.error("Bad Request! User already exists ");
            return Response.status(400).build();
        }
        User user = new User();
        user.setUsername(binding.getUsername());
        user.setFullname(binding.getFullname());
        user.setEmail(binding.getEmail());
        user.setPhone(binding.getPhone());
        user.setAddress(binding.getAddress());
        user.setAccountEnabled(true);
        user.setDateOfbirth(binding.getDateOfBirth());
        user.grantRole("USER");
        String salt = BCrypt.gensalt();
        user.setSalt(salt);
        String password = binding.getPassword();
        user.setPassword(BCrypt.hashpw(password, salt));
        binding.setPassword(password);
        userService.save(user);
        return Response.ok().build();
    }

    @POST
    @Path(value = "forgotPass")
    @Produces(MediaType.APPLICATION_JSON)
    public Response changePassword(UserBinding binding) {
        User existUser = userService.findUserByEmail(binding.getEmail());
        if(null != existUser) {
            if (existUser.getUsername().equals(binding.getUsername())) {
                String salt = BCrypt.gensalt();
                existUser.setSalt(salt);
                String password = binding.getPassword();
                existUser.setPassword(BCrypt.hashpw(password, salt));
                binding.setPassword(password);
                userService.updateUser(existUser);
                return Response.ok().build();
            } else {
                LOG.error("Bad credentials ");
                return Response.status(400).build();
            }
        }
        else{
            LOG.error("Bad credentials ");
            return Response.status(404).build();
        }
    }
}

package ontology.service;

public abstract class OntoService<T> {
    public abstract T save(T t);
    public abstract void delete(T t);
    public abstract T find(long id);
}

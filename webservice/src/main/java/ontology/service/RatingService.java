package ontology.service;

import ontology.domain.Rating;
import ontology.domain.User;
import ontology.repository.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Igor Sila
 */
@Service
@Transactional
public class RatingService extends OntoService<Rating> {
    @Autowired
    private RatingRepository ratingRepository;

    @Override
    public Rating save(Rating rating) {
        return ratingRepository.save(rating);
    }

    @Override
    public void delete(Rating rating) {
        ratingRepository.delete(rating);
    }

    @Override
    public Rating find(long id) {
        return ratingRepository.findOne(id);
    }

    public List<Rating> findUserRating(User user){
        return ratingRepository.findUserRating(user);
    }

    public List<Rating> findAll(){
        return (List<Rating>) ratingRepository.findAll();
    }
}

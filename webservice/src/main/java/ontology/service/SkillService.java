package ontology.service;

import ontology.domain.Skill;
import ontology.repository.SkillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Igor Sila
 */
@Service
@Transactional
public class SkillService extends OntoService<Skill> {
    @Autowired
    private SkillRepository skillRepository;

    @Override
    public Skill save(Skill skill) {
        return skillRepository.save(skill);
    }

    @Override
    public void delete(Skill skill) {
        skillRepository.delete(skill);
    }

    @Override
    public Skill find(long id) {
        return skillRepository.findOne(id);
    }
}

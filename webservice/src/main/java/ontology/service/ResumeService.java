package ontology.service;

import ontology.domain.Resume;
import ontology.repository.ResumeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class ResumeService extends OntoService<Resume> {

    @Autowired
    private ResumeRepository resumeRepository;

    @Override
    public Resume save(Resume resume) {
        return resumeRepository.save(resume);
    }

    @Override
    public void delete(Resume resume) {
        resumeRepository.delete(resume);
    }

    @Override
    public Resume find(long id) {
        return resumeRepository.findOne(id);
    }
}

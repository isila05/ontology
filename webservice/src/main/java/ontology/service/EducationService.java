package ontology.service;

import ontology.domain.Education;
import ontology.repository.EducationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Igor Sila
 */
@Service
@Transactional
public class EducationService extends OntoService<Education> {
    @Autowired
    private EducationRepository educationRepository;

    @Override
    public Education save(Education education) {
        return educationRepository.save(education);
    }

    @Override
    public void delete(Education education) {
        educationRepository.delete(education);
    }

    @Override
    public Education find(long id) {
        return educationRepository.findOne(id);
    }
}

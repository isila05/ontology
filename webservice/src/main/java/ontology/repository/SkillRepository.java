package ontology.repository;

import ontology.domain.Skill;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Igor Sila
 */
@Repository
public interface SkillRepository extends CrudRepository<Skill, Long> {
}
package ontology.repository;

import ontology.domain.Education;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Igor Sila
 */
@Repository
public interface EducationRepository extends CrudRepository<Education, Long> {
}


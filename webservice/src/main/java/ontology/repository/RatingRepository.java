package ontology.repository;

import ontology.domain.Rating;
import ontology.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Igor Sila
 */
@Repository
public interface RatingRepository extends CrudRepository<Rating, Long> {

    @Query("select r from ontology.domain.Rating as r where r.user=:user")
    List<Rating> findUserRating(@Param("user") User user);
}

package ontology.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Igor Sila
 */
@Entity
@Table(name = "fuser", schema = "public")
public class User {
    @Id
    @SequenceGenerator(name = "UserSeq", sequenceName = "fuser_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UserSeq")
    private Long id;

    @NotNull
    private String username;

    private String fullname;

    private String email;

    @NotNull
    private String password;

    private String salt;

    private String address;

    private String phone;

    private Calendar dateOfbirth;

    @Transient
    private long expires;

    @NotNull
    private boolean accountExpired;

    @NotNull
    private boolean accountLocked;

    @NotNull
    private boolean credentialsExpired;

    @NotNull
    private boolean accountEnabled;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "user", orphanRemoval = true)
    private Set<UserAuthority> authorities;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "user", orphanRemoval = true)
    private Set<Resume> resumes;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user", orphanRemoval = true)
    private Set<Rating> ratings;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isAccountNonExpired() {
        return !accountExpired;
    }

    public void setAccountExpired(boolean accountExpired) {
        this.accountExpired = accountExpired;
    }

    public boolean isAccountNonLocked() {
        return !accountLocked;
    }

    public void setAccountLocked(boolean accountLocked) {
        this.accountLocked = accountLocked;
    }

    public boolean isCredentialsNonExpired() {
        return !credentialsExpired;
    }

    public void setCredentialsExpired(boolean credentialsExpired) {
        this.credentialsExpired = credentialsExpired;
    }

    public boolean isEnabled() {
        return accountEnabled;
    }

    public void setAccountEnabled(boolean accountEnabled) {
        this.accountEnabled = accountEnabled;
    }

    public long getExpires() {
        return expires;
    }

    public void setExpires(long expires) {
        this.expires = expires;
    }

    public Set<UserAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<UserAuthority> authorities) {
        this.authorities = authorities;
    }

    public Set<Resume> getResumes() {
        return resumes;
    }

    public void setResumes(Set<Resume> resumes) {
        this.resumes = resumes;
    }

    public Set<String> getRoles() {
        Set<String> roles = new HashSet<>();
        if (authorities != null) {
            for (UserAuthority authority : authorities) {
                String role = authority.getAuthority();
                if (role != null) {
                    roles.add(role);
                } else {
                    throw new IllegalArgumentException("No role defined : " + role);
                }
            }
        }
        return roles;
    }

    public void grantRole(String role) {
        if (authorities == null) {
            authorities = new HashSet<UserAuthority>();
        }
        authorities.add(asAuthorityFor(role));
    }

    public void revokeRole(String role) {
        if (authorities != null) {
            authorities.remove(asAuthorityFor(role));
        }
    }

    public boolean hasRole(String role) {
        return authorities.contains(asAuthorityFor(role));
    }

    private UserAuthority asAuthorityFor (String role){
        final UserAuthority authority = new UserAuthority();
        authority.setAuthority("ROLE_" + role);
        authority.setUser(this);
        return authority;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Calendar getDateOfbirth() {
        return dateOfbirth;
    }

    public void setDateOfbirth(Calendar dateOfbirth) {
        this.dateOfbirth = dateOfbirth;
    }

    public Set<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(Set<Rating> ratings) {
        this.ratings = ratings;
    }
}


package ontology.domain;

import javax.persistence.*;

/**
 * @author Igor Sila
 */
@Entity
@Table(name = "rating", schema = "public")
public class Rating {
    @Id
    @SequenceGenerator(name = "RatingSeq", sequenceName = "rating_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RatingSeq")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    private String parent;

    private Long count;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}

package ontology.domain;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

/**
 * @author Igor Sila
 */
@Entity
@Table(name = "authorities", schema = "public")
public class UserAuthority implements GrantedAuthority {
    @Id
    @SequenceGenerator(name = "UserAuthoritySeq", sequenceName = "authorities_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UserAuthoritySeq")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    private String authority;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}

package ontology.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author Igor Sila
 */
@Configuration
@EnableJpaRepositories("ontology.repository")
public class RepositoryConfig {
}

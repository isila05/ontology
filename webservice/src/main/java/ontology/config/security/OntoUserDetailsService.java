package ontology.config.security;

import ontology.domain.User;
import ontology.domain.UserAuthority;
import ontology.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Igor Sila
 */
@Service
public class OntoUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    private static final Logger LOG = LoggerFactory.getLogger(OntoUserDetailsService.class);

    private final AccountStatusUserDetailsChecker detailsChecker = new AccountStatusUserDetailsChecker();

    @Override
    public final OntoUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("user not found");
        }
        OntoUserDetails userDetails = new OntoUserDetails();

        userDetails.setUsername(user.getUsername());
        userDetails.setPassword(user.getPassword());
        userDetails.setAccountNonExpired(user.isAccountNonExpired());
        userDetails.setAccountNonLocked(user.isAccountNonLocked());
        userDetails.setCredentialsNonExpired(user.isCredentialsNonExpired());
        userDetails.setEnabled(user.isEnabled());

        Set<OntoGruntedAuthority> authorities = new HashSet<>();
        for (UserAuthority authority : user.getAuthorities()) {
            OntoGruntedAuthority ontoGruntedAuthority = new OntoGruntedAuthority(authority.getAuthority());
            authorities.add(ontoGruntedAuthority);
        }
        userDetails.setAuthorities(authorities);

        detailsChecker.check(userDetails);

        return userDetails;
    }

}

package ontology.config.security;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author Igor Sila
 */
@JsonAutoDetect
public class OntoGruntedAuthority implements GrantedAuthority {
    private String authority;

    public OntoGruntedAuthority() {
    }

    public OntoGruntedAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return authority;
    }
}

package ontology.bindings;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Igor Sila
 */
@XmlRootElement
public class OntologyBinding {
    private String biography;

    private String skill;

    private String parent;

    private Boolean createParent;

    private String username;

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public Boolean getCreateParent() {
        return createParent;
    }

    public void setCreateParent(Boolean createParent) {
        this.createParent = createParent;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

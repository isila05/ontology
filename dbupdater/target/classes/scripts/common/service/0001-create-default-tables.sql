--liquibase formatted sql
--changeset sila:01
CREATE TABLE IF NOT EXISTS "fuser" (
  id                  BIGSERIAL PRIMARY KEY,
  username            CHARACTER VARYING,
  fullname            CHARACTER VARYING,
  email               CHARACTER VARYING,
  password            CHARACTER VARYING,
  salt                CHARACTER VARYING,
  address             CHARACTER VARYING,
  phone               CHARACTER VARYING,
  dateOfBirth         TIMESTAMP WITH TIME ZONE,
  accountexpired      BOOLEAN NOT NULL,
  accountenabled      BOOLEAN NOT NULL,
  accountlocked       BOOLEAN NOT NULL,
  credentialsExpired  BOOLEAN NOT NULL
);

CREATE TABLE IF NOT EXISTS  "authorities" (
  id             BIGSERIAL PRIMARY KEY,
  user_id        BIGSERIAL NOT NULL,
  authority      CHARACTER VARYING
);

CREATE TABLE IF NOT EXISTS  "resume" (
  id              BIGSERIAL PRIMARY KEY,
  user_id         BIGSERIAL NOT NULL,
  additional_info CHARACTER VARYING
);

CREATE TABLE IF NOT EXISTS  "education" (
  id               BIGSERIAL PRIMARY KEY,
  resume_id        BIGSERIAL NOT NULL,
  education_type   CHARACTER VARYING,
  value            CHARACTER VARYING
);

CREATE TABLE IF NOT EXISTS  "skill" (
  id               BIGSERIAL PRIMARY KEY,
  resume_id        BIGSERIAL NOT NULL,
  parent           CHARACTER VARYING,
  value            CHARACTER VARYING
);

CREATE TABLE IF NOT EXISTS  "rating" (
  id               BIGSERIAL PRIMARY KEY,
  user_id          BIGSERIAL NOT NULL,
  parent           CHARACTER VARYING,
  count            BIGSERIAL
);